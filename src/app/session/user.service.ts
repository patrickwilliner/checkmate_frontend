import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { User } from './user.model';

@Injectable()
export class UserService {
  constructor(private http: Http) {
  }

  createUser(user: User) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = {
        headers: headers
    };

    return this.http.post('http://localhost:8080/api/users/', JSON.stringify(user), options).map((res: Response) => {
      return res.json();
    }).catch(this.handleError);
  }

  private handleError(error: any) {
        var applicationError = error.headers.get('Application-Error');
        var serverError = error.json();
        var modelStateErrors: string = '';

        if (!serverError.type) {
            console.log(serverError);
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }

        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;

        return Observable.throw(applicationError || modelStateErrors || 'Server error');
    }
}
