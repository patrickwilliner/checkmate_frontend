import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { User } from './user.model';
import { NotificationService } from '../util/notification.service';

@Injectable()
export class SessionService {
  public sessionUser: User;

  constructor(private http: Http, private notificationService: NotificationService) {
    //this.loadSessionUser();
  }

  authenticate(login: string, password: string) {
    let url = '/api/authenticate';
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let body = `username=${login}&password=${password}`;

    return this.http.post(url, body, {headers:headers}).map((res:Response) => {
      this.loadSessionUser();
    });
  }

  logout() {
    let url = '/api/logout';

    return this.http.post(url, {}).map((res:Response) => {
      delete this.sessionUser;
      this.notificationService.notifyAll('session:changed');
    });
  }

  private loadSessionUser() {
    let url = '/api/session';
    return this.http.get(url).subscribe(res => {
      this.sessionUser = new User(res.json().firstName, res.json().lastName, res.json().nickname);
      this.notificationService.notifyAll('session:changed');
    });
  }
}
