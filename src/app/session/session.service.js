"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var user_model_1 = require("./user.model");
var notification_service_1 = require("../util/notification.service");
var SessionService = (function () {
    function SessionService(http, notificationService) {
        this.http = http;
        this.notificationService = notificationService;
        //this.loadSessionUser();
    }
    SessionService.prototype.authenticate = function (login, password) {
        var _this = this;
        var url = '/api/authenticate';
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        var body = "username=" + login + "&password=" + password;
        return this.http.post(url, body, { headers: headers }).map(function (res) {
            _this.loadSessionUser();
        });
    };
    SessionService.prototype.logout = function () {
        var _this = this;
        var url = '/api/logout';
        return this.http.post(url, {}).map(function (res) {
            delete _this.sessionUser;
            _this.notificationService.notifyAll('session:changed');
        });
    };
    SessionService.prototype.loadSessionUser = function () {
        var _this = this;
        var url = '/api/session';
        return this.http.get(url).subscribe(function (res) {
            _this.sessionUser = new user_model_1.User(res.json().firstName, res.json().lastName, res.json().nickname);
            _this.notificationService.notifyAll('session:changed');
        });
    };
    return SessionService;
}());
SessionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, notification_service_1.NotificationService])
], SessionService);
exports.SessionService = SessionService;
//# sourceMappingURL=session.service.js.map