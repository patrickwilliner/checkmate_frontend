"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = (function () {
    function User(firstName, lastName, nickname) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.model.js.map