import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { SessionService } from './session.service';
import { UserService } from './user.service';

@NgModule({
  imports: [HttpModule],
  providers: [SessionService, UserService]
})
export class SessionModule {
}
