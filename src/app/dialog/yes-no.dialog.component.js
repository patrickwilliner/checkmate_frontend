"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var YesNoDialogComponent = (function () {
    function YesNoDialogComponent() {
        this.yesBtnLabel = "Yes";
        this.noBtnLabel = "No";
    }
    return YesNoDialogComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], YesNoDialogComponent.prototype, "title", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], YesNoDialogComponent.prototype, "yesBtnLabel", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], YesNoDialogComponent.prototype, "noBtnLabel", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Properties)
], YesNoDialogComponent.prototype, "properties", void 0);
YesNoDialogComponent = __decorate([
    core_1.Component({
        selector: '[cm-yes-no-dialog]',
        template: "\n      <div class=\"modal-header\" *ngIf=\"properties.showHeader\">\n        <h4 class=\"modal-title\">{{title}}</h4>\n        <button type=\"button\" class=\"close\">\n          <span>&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <ng-content></ng-content>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"properties.yesCb()\">{{yesBtnLabel}}</button>\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"properties.noCb()\">{{noBtnLabel}}</button>\n      </div>\n  "
    })
], YesNoDialogComponent);
exports.YesNoDialogComponent = YesNoDialogComponent;
var Properties = (function () {
    function Properties() {
        this.showHeader = true;
    }
    return Properties;
}());
exports.Properties = Properties;
//# sourceMappingURL=yes-no.dialog.component.js.map