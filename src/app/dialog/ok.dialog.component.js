"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var OkDialogComponent = (function () {
    function OkDialogComponent() {
        this.okBtnLabel = "OK";
    }
    return OkDialogComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], OkDialogComponent.prototype, "title", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], OkDialogComponent.prototype, "okBtnLabel", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], OkDialogComponent.prototype, "form", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Properties)
], OkDialogComponent.prototype, "properties", void 0);
OkDialogComponent = __decorate([
    core_1.Component({
        selector: '[cm-ok-dialog]',
        template: "\n      <div class=\"modal-header\" *ngIf=\"properties.showHeader\">\n        <h4 class=\"modal-title\">{{title}}</h4>\n        <button type=\"button\" class=\"close\">\n          <span>&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <div cm-error-message [form]=\"form\"></div>\n        <ng-content></ng-content>\n      </div>\n      <div class=\"modal-footer\" *ngIf=\"properties.showFooter\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"properties.okCb()\">{{okBtnLabel}}</button>\n      </div>\n  "
    })
], OkDialogComponent);
exports.OkDialogComponent = OkDialogComponent;
var Properties = (function () {
    function Properties() {
        this.showHeader = true;
        this.showFooter = true;
    }
    return Properties;
}());
exports.Properties = Properties;
//# sourceMappingURL=ok.dialog.component.js.map