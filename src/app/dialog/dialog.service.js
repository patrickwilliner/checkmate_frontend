"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ReplaySubject_1 = require("rxjs/ReplaySubject");
var DialogService = (function () {
    function DialogService(compiler) {
        this.compiler = compiler;
        this.activeInstances = 0;
    }
    DialogService.prototype.registerViewContainerRef = function (vcRef) {
        if (this.vcRef) {
            throw new Error('ViewConteinerRef registered already');
        }
        else {
            this.vcRef = vcRef;
        }
    };
    DialogService.prototype.registerInjector = function (injector) {
        if (this.injector) {
            throw new Error('Injector registered already');
        }
        else {
            this.injector = injector;
        }
    };
    DialogService.prototype.showOkDialog = function () {
    };
    DialogService.prototype.showYesNoDialog = function () {
    };
    DialogService.prototype.showBlankDialog = function () {
    };
    DialogService.prototype.create = function (module, component, parameters) {
        var _this = this;
        // we return a stream so we can  access the componentref
        var componentRef$ = new ReplaySubject_1.ReplaySubject();
        // compile the component based on its type and
        // create a component factory
        this.compiler.compileModuleAndAllComponentsAsync(module)
            .then(function (factory) {
            // look for the componentfactory in the modulefactory
            var componentFactory = factory.componentFactories
                .filter(function (item) { return item.componentType === component; })[0];
            // the injector will be needed for DI in 
            // the custom component
            var childInjector = core_1.ReflectiveInjector
                .resolveAndCreate([], _this.injector);
            // create the actual component
            var componentRef = _this.vcRef
                .createComponent(componentFactory, 0, childInjector);
            // pass the @Input parameters to the instance
            Object.assign(componentRef.instance, parameters);
            _this.activeInstances++;
            // add a destroy method to the modal instance
            componentRef.instance["destroy"] = function () {
                _this.activeInstances--;
                // this will destroy the component
                componentRef.destroy();
            };
            // the component is rendered into the ViewContainerRef
            // so we can update and complete the stream
            componentRef$.next(componentRef);
            componentRef$.complete();
        });
        return componentRef$;
    };
    return DialogService;
}());
DialogService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Compiler])
], DialogService);
exports.DialogService = DialogService;
//# sourceMappingURL=dialog.service.js.map