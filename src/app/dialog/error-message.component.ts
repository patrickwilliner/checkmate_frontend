import { Component, Input } from '@angular/core';

import { IFormData } from '../util/form-data.model';

@Component({
  selector: '[cm-error-message]',
  template: `
    <div *ngIf="form && form.errorMessage" class="row">
      <div class="col-12">
        <div class="alert alert-danger" role="alert">{{form.errorMessage}}</div>
      </div>
    </div>
  `
})
export class ErrorMessageComponent {
  @Input() form: IFormData;
}
