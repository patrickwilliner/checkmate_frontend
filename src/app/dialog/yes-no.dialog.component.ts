import { Component, Input } from '@angular/core';

@Component({
  selector: '[cm-yes-no-dialog]',
  template: `
      <div class="modal-header" *ngIf="properties.showHeader">
        <h4 class="modal-title">{{title}}</h4>
        <button type="button" class="close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ng-content></ng-content>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" (click)="properties.yesCb()">{{yesBtnLabel}}</button>
        <button type="button" class="btn btn-secondary" (click)="properties.noCb()">{{noBtnLabel}}</button>
      </div>
  `
})
export class YesNoDialogComponent {
  @Input() private title: string;
  @Input() private yesBtnLabel = "Yes"
  @Input() private noBtnLabel = "No"
  @Input() private properties: Properties;
}

export class Properties {
  public yesCb: () => void;
  public noCb: () => void;
  public showHeader = true;
}
