"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialog_service_1 = require("./dialog.service");
var ModalPlaceholderComponent = (function () {
    function ModalPlaceholderComponent(modalService, injector) {
        this.modalService = modalService;
        this.injector = injector;
    }
    ModalPlaceholderComponent.prototype.ngOnInit = function () {
        this.modalService.registerViewContainerRef(this.viewContainerRef);
        this.modalService.registerInjector(this.injector);
    };
    return ModalPlaceholderComponent;
}());
__decorate([
    core_1.ViewChild("modalplaceholder", { read: core_1.ViewContainerRef }),
    __metadata("design:type", Object)
], ModalPlaceholderComponent.prototype, "viewContainerRef", void 0);
ModalPlaceholderComponent = __decorate([
    core_1.Component({
        selector: '[modal-placeholder]',
        template: "<div #modalplaceholder></div>"
    }),
    __metadata("design:paramtypes", [dialog_service_1.DialogService,
        core_1.Injector])
], ModalPlaceholderComponent);
exports.ModalPlaceholderComponent = ModalPlaceholderComponent;
//# sourceMappingURL=modalPlaceholder.component.js.map