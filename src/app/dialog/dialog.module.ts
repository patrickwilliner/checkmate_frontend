import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DialogService } from './dialog.service';

import { ErrorMessageComponent } from './error-message.component';
import { OkDialogComponent } from './ok.dialog.component';
import { YesNoDialogComponent } from './yes-no.dialog.component';
import { BlankDialogComponent } from './blank.dialog.component';

@NgModule({
  imports: [CommonModule],
  declarations: [OkDialogComponent, YesNoDialogComponent, ErrorMessageComponent, BlankDialogComponent],
  exports: [OkDialogComponent, YesNoDialogComponent],
  providers: [DialogService]
})
export class DialogModule {
}
