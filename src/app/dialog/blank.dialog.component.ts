import { Component } from '@angular/core';

@Component({
    selector: 'cm-blank-dialog',
    template: `
        <div class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">XXXXXX</h4>
                        <button type="button" class="close">
                          <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        XXXXXXX
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary">XXXX</button>
                    </div>
                </div>
            </div>
        </div>
    `
})
export class BlankDialogComponent {
}