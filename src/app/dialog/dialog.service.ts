import { Injectable, ViewContainerRef, Injector, Compiler, ComponentRef, ReflectiveInjector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class DialogService {
    private vcRef: ViewContainerRef; 
    private injector: Injector; 

    public activeInstances: number = 0;
    
    constructor(private compiler: Compiler) {
    }

    registerViewContainerRef(vcRef: ViewContainerRef): void {
        if (this.vcRef) {
            throw new Error('ViewConteinerRef registered already');
        } else {
            this.vcRef = vcRef;
        }
    }

    registerInjector(injector: Injector): void {
        if (this.injector) {
            throw new Error('Injector registered already');
        } else {
            this.injector = injector;
        }
    }

    showOkDialog() {
        
    }

    showYesNoDialog() {

    }

    showBlankDialog() {

    }

    public create<T>(module: any, component: any, parameters?: Object): Observable<ComponentRef<T>> {
     	// we return a stream so we can  access the componentref
        let componentRef$ = new ReplaySubject(); 
        // compile the component based on its type and
        // create a component factory
        this.compiler.compileModuleAndAllComponentsAsync(module)
            .then(factory => {
            	// look for the componentfactory in the modulefactory
             	let componentFactory = factory.componentFactories
             		.filter(item => item.componentType === component)[0];
            	// the injector will be needed for DI in 
            	// the custom component
                const childInjector = ReflectiveInjector
                	.resolveAndCreate([], this.injector);
            	// create the actual component
                let componentRef = this.vcRef
                	.createComponent(componentFactory, 0, childInjector);
                // pass the @Input parameters to the instance
                Object.assign(componentRef.instance, parameters); 
                this.activeInstances ++;
                // add a destroy method to the modal instance
                componentRef.instance["destroy"] = () => {
                    this.activeInstances --;
                    // this will destroy the component
                    componentRef.destroy(); 
                };
                // the component is rendered into the ViewContainerRef
                // so we can update and complete the stream
                componentRef$.next(componentRef);
                componentRef$.complete();
            });
        return componentRef$;
    }
}