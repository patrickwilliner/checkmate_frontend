"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BlankDialogComponent = (function () {
    function BlankDialogComponent() {
    }
    return BlankDialogComponent;
}());
BlankDialogComponent = __decorate([
    core_1.Component({
        selector: 'cm-blank-dialog',
        template: "\n        <div class=\"modal fade\">\n            <div class=\"modal-dialog\" role=\"document\">\n                <div class=\"modal-content\">\n                    <div class=\"modal-header\">\n                        <h4 class=\"modal-title\">XXXXXX</h4>\n                        <button type=\"button\" class=\"close\">\n                          <span>&times;</span>\n                        </button>\n                    </div>\n                    <div class=\"modal-body\">\n                        XXXXXXX\n                    </div>\n                    <div class=\"modal-footer\">\n                        <button type=\"button\" class=\"btn btn-secondary\">XXXX</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    "
    })
], BlankDialogComponent);
exports.BlankDialogComponent = BlankDialogComponent;
//# sourceMappingURL=blank.dialog.component.js.map