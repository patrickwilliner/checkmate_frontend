import { Component, Input } from '@angular/core';

import { IFormData } from '../util/form-data.model';

@Component({
  selector: '[cm-ok-dialog]',
  template: `
      <div class="modal-header" *ngIf="properties.showHeader">
        <h4 class="modal-title">{{title}}</h4>
        <button type="button" class="close">
          <span>&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div cm-error-message [form]="form"></div>
        <ng-content></ng-content>
      </div>
      <div class="modal-footer" *ngIf="properties.showFooter">
        <button type="button" class="btn btn-secondary" (click)="properties.okCb()">{{okBtnLabel}}</button>
      </div>
  `
})
export class OkDialogComponent {
  @Input() private title: string;
  @Input() private okBtnLabel = "OK"
  @Input() private form: IFormData;
  @Input() private properties: Properties;
}

export class Properties {
  public okCb: () => void;
  public showHeader = true;
  public showFooter = true;
}
