"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var dialog_service_1 = require("./dialog.service");
var error_message_component_1 = require("./error-message.component");
var ok_dialog_component_1 = require("./ok.dialog.component");
var yes_no_dialog_component_1 = require("./yes-no.dialog.component");
var blank_dialog_component_1 = require("./blank.dialog.component");
var DialogModule = (function () {
    function DialogModule() {
    }
    return DialogModule;
}());
DialogModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule],
        declarations: [ok_dialog_component_1.OkDialogComponent, yes_no_dialog_component_1.YesNoDialogComponent, error_message_component_1.ErrorMessageComponent, blank_dialog_component_1.BlankDialogComponent],
        exports: [ok_dialog_component_1.OkDialogComponent, yes_no_dialog_component_1.YesNoDialogComponent],
        providers: [dialog_service_1.DialogService]
    })
], DialogModule);
exports.DialogModule = DialogModule;
//# sourceMappingURL=dialog.module.js.map