"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var logger_service_1 = require("./logger.service");
var elapsed_time_pipe_1 = require("./elapsed-time.pipe");
var notification_service_1 = require("./notification.service");
var UtilModule = (function () {
    function UtilModule() {
    }
    return UtilModule;
}());
UtilModule = __decorate([
    core_1.NgModule({
        declarations: [elapsed_time_pipe_1.ElapsedTimePipe],
        providers: [logger_service_1.LoggerService, notification_service_1.NotificationService],
        exports: [elapsed_time_pipe_1.ElapsedTimePipe]
    })
], UtilModule);
exports.UtilModule = UtilModule;
//# sourceMappingURL=util.module.js.map