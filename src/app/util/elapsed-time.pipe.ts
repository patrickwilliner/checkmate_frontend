import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({name: 'elapsedTime'})
export class ElapsedTimePipe implements PipeTransform {
  transform(value: number): string {
    value = _.isNil(value) ? 0 : value;
    let hours = Math.floor(value / 3600);
    let minutes = Math.floor(value / 60) - hours * 60;
    let seconds = value % 60;

    return (hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);
  }
}
