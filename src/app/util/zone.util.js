"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LeaveZoneScheduler = (function () {
    function LeaveZoneScheduler(zone, scheduler) {
        this.zone = zone;
        this.scheduler = scheduler;
    }
    LeaveZoneScheduler.prototype.schedule = function () {
        var _this = this;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return this.zone.runOutsideAngular(function () {
            return _this.scheduler.schedule.apply(_this.scheduler, args);
        });
    };
    return LeaveZoneScheduler;
}());
var EnterZoneScheduler = (function () {
    function EnterZoneScheduler(zone, scheduler) {
        this.zone = zone;
        this.scheduler = scheduler;
    }
    EnterZoneScheduler.prototype.schedule = function () {
        var _this = this;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return this.zone.run(function () {
            return _this.scheduler.schedule.apply(_this.scheduler, args);
        });
    };
    return EnterZoneScheduler;
}());
function leaveZone(zone, scheduler) {
    return new LeaveZoneScheduler(zone, scheduler);
}
exports.leaveZone = leaveZone;
function enterZone(zone, scheduler) {
    return new EnterZoneScheduler(zone, scheduler);
}
exports.enterZone = enterZone;
//# sourceMappingURL=zone.util.js.map