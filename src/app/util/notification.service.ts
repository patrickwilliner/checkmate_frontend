import { Injectable } from '@angular/core';

@Injectable()
export class NotificationService {
  private listeners = {};

  addListener(event: string, listener: Listener) {
    if (!this.listeners[event]) {
      this.listeners[event] = [];
    }

    this.listeners[event].push(listener);
  }

  removeListener(event: string, listener: Listener) {

  }

  notifyAll(event: string) {
    if (this.listeners[event]) {
      for (let listener of this.listeners[event]) {
        listener.notify();
      }
    }
  }
}

export interface Listener {
  notify(): void;
}
