import { NgModule } from '@angular/core';

import { LoggerService } from './logger.service';
import { ElapsedTimePipe } from './elapsed-time.pipe';
import { NotificationService } from './notification.service';

@NgModule({
  declarations: [ElapsedTimePipe],
  providers: [LoggerService, NotificationService],
  exports: [ElapsedTimePipe]
})
export class UtilModule {
}
