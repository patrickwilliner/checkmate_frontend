import { Injectable } from '@angular/core';

enum LogLevel {
  INFO,
  WARN,
  ERROR
}

@Injectable()
export class LoggerService {
  public info(message: string): void {
    console.info("**", message);
  }

  public warn(message: string): void {
    console.warn("**", message);
  }

  public error(message: string): void {
    console.error("**", message);
  }
}
