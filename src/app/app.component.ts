import { Component } from '@angular/core';
import { HostListener } from '@angular/core';

import { SessionService } from './session/session.service';
import { NotificationService } from './util/notification.service';
import { ChessboardService } from './chessboard/chessboard.service';

@Component({
  selector: '[cm-chess-app]',
  template: `
    <div cm-modal-placeholder></div>
    <div cm-navigation-bar></div>

    <div class="container">
      <div class="row">
        <div class="col-12">&nbsp;</div>
      </div>

      <div class="row">
        <div class="col-3">
          <div class="card overview">
            <div class="card-block">
              <h4 class="card-title">Overview</h4>
              <div cm-current-player></div>
              <div cm-timer></div>
            </div>
          </div>
        </div>

        <div class="col-6">
          <div cm-chessboard></div>
        </div>

        <div class="col-3">
          <div class="card history">
            <div class="card-block">
              <h4 class="card-title">History</h4>
              <div cm-history></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class AppComponent {
  constructor(private chessboardService: ChessboardService) {
  }

  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    if (event.key === 'n' && event.ctrlKey) {
      this.chessboardService.newGame();
    }
  }
}
