import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtilModule } from '../util/util.module';

import { CurrentPlayerComponent } from './current-player.component';
import { TimerComponent } from './timer.component';
import { HistoryComponent } from './history.component';

@NgModule({
  imports: [CommonModule, UtilModule],
  declarations: [CurrentPlayerComponent, TimerComponent, HistoryComponent],
  exports: [CurrentPlayerComponent, TimerComponent, HistoryComponent]
})
export class GameModule {
}
