"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var chessboard_service_1 = require("../chessboard/chessboard.service");
var CurrentPlayerComponent = (function () {
    function CurrentPlayerComponent(chessboardService) {
        this.chessboardService = chessboardService;
    }
    CurrentPlayerComponent.prototype.getCurrentPlayer = function () {
        var chessboard = this.chessboardService.getChessboard();
        return chessboard ? chessboard.activeColor.name : '-';
    };
    return CurrentPlayerComponent;
}());
CurrentPlayerComponent = __decorate([
    core_1.Component({
        selector: '[cm-current-player]',
        template: "<p>Next move: {{getCurrentPlayer()}}</p>"
    }),
    __metadata("design:paramtypes", [chessboard_service_1.ChessboardService])
], CurrentPlayerComponent);
exports.CurrentPlayerComponent = CurrentPlayerComponent;
//# sourceMappingURL=current-player.component.js.map