import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { ChessboardService } from '../chessboard/chessboard.service';

@Component({
  selector: '[cm-history]',
  template: `
    <ol class="two-columns">
      <li *ngFor="let historyItem of chessboardService.getChessboard().history trackBy: index">{{historyItem.toString()}}</li>
    </ol>
  `
})
export class HistoryComponent {
  constructor(private chessboardService: ChessboardService) {
  }
}
