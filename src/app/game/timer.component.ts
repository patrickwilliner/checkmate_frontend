import { Component, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { ChessboardService } from '../chessboard/chessboard.service';

@Component({
  selector: '[cm-timer]',
  template: `<p>Time: {{currentTime | elapsedTime}}</p>`
})
export class TimerComponent {
  private currentTime: string;

  constructor(private chessboardService: ChessboardService, private ngZone: NgZone) {
    ngZone.runOutsideAngular(() => {
      setInterval(() => {
        ngZone.run(() => {
          this.chessboardService.getChessboard().timer.subject.subscribe(t => this.currentTime = '' + t);
        });
      }, 2500);
    });
  }
}
