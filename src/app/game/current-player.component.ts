import { Component } from '@angular/core';

import { ChessboardService } from '../chessboard/chessboard.service';

@Component({
  selector: '[cm-current-player]',
  template: `<p>Next move: {{getCurrentPlayer()}}</p>`
})
export class CurrentPlayerComponent {
  constructor(private chessboardService: ChessboardService) {
  }

  getCurrentPlayer() {
    let chessboard = this.chessboardService.getChessboard();
    return chessboard ? chessboard.activeColor.name : '-';
  }
}
