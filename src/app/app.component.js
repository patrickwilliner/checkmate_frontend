"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: '[cm-chess-app]',
        template: "\n    <div cm-modal-placeholder></div>\n    <div cm-navigation-bar></div>\n\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-12\">&nbsp;</div>\n      </div>\n\n      <div class=\"row\">\n        <div class=\"col-3\">\n          <div class=\"card overview\">\n            <div class=\"card-block\">\n              <h4 class=\"card-title\">Overview</h4>\n              <div cm-current-player></div>\n              <div cm-timer></div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"col-6\">\n          <div cm-chessboard></div>\n        </div>\n\n        <div class=\"col-3\">\n          <div class=\"card history\">\n            <div class=\"card-block\">\n              <h4 class=\"card-title\">History</h4>\n              <div cm-history></div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  "
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map