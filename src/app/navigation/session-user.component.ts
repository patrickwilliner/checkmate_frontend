import { Component } from '@angular/core';

import { User } from '../session/user.model';
import { NotificationService, Listener } from '../util/notification.service';
import { SessionService } from '../session/session.service';

@Component({
  selector: '[cm-session-user]',
  template: `
    <span class="fa fa-user" *ngIf="vm.user">&nbsp;{{vm.user.nickname}}</span>
  `
})
export class SessionUserComponent implements Listener {
  private vm = new Vm();

  constructor(private notificationService: NotificationService, private sessionService: SessionService) {
    notificationService.addListener('session:changed', this);
  }

  notify() {
    this.vm.user = this.sessionService.sessionUser;
  }
}

class Vm {
  user: User;
}
