import { Component } from '@angular/core';

@Component({
  selector: '[cm-statistics-menu]',
  template: `
    <a class="nav-link dropdown-toggle disabled" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown">
      Statistics
    </a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#">Highscores</a>
    </div>
  `
})
export class StatisticsMenuComponent {
}
