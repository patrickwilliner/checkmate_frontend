import { NgModule } from '@angular/core';

import { StatisticsMenuComponent } from './statistics.menu.component';

@NgModule({
  declarations: [StatisticsMenuComponent],
  exports: [StatisticsMenuComponent]
})
export class StatisticsMenuModule {
}
