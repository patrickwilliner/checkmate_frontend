import { Component } from '@angular/core';

@Component({
  selector: '[cm-help-menu]',
  template: `
    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Help
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <div cm-about-menu></div>
    </div>
  `
})
export class HelpMenuComponent {
}
