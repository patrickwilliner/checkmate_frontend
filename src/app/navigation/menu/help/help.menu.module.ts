import { NgModule } from '@angular/core';

import { DialogModule } from '../../../dialog/dialog.module';

import { HelpMenuComponent } from './help.menu.component';
import { AboutMenuComponent } from './about.menu.component';

@NgModule({
  imports: [DialogModule],
  declarations: [HelpMenuComponent, AboutMenuComponent],
  exports: [HelpMenuComponent]
})
export class HelpMenuModule {
}
