"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var ok_dialog_component_1 = require("../../../dialog/ok.dialog.component");
var AboutMenuComponent = (function () {
    function AboutMenuComponent(modalService) {
        var _this = this;
        this.modalService = modalService;
        this.okDialogProperties = new ok_dialog_component_1.Properties();
        this.okCb = function () {
            _this.modal.dismiss();
        };
        this.okDialogProperties.okCb = this.okCb;
    }
    AboutMenuComponent.prototype.open = function (content) {
        this.modal = this.modalService.open(content);
    };
    return AboutMenuComponent;
}());
AboutMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-about-menu]',
        template: "\n    <ng-template #content>\n      <div cm-ok-dialog title=\"About\" okBtnLabel=\"Close\" [properties]=\"okDialogProperties\">\n        Check Mate chess game.\n      </div>\n    </ng-template>\n    <a class=\"dropdown-item\" href=\"#\" (click)=\"open(content)\">About</a>\n  "
    }),
    __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal])
], AboutMenuComponent);
exports.AboutMenuComponent = AboutMenuComponent;
//# sourceMappingURL=about.menu.component.js.map