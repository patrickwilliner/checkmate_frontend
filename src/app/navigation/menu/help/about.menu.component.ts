import { Component } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { Properties } from '../../../dialog/ok.dialog.component';

@Component({
  selector: '[cm-about-menu]',
  template: `
    <ng-template #content>
      <div cm-ok-dialog title="About" okBtnLabel="Close" [properties]="okDialogProperties">
        Check Mate chess game.
      </div>
    </ng-template>
    <a class="dropdown-item" href="#" (click)="open(content)">About</a>
  `
})
export class AboutMenuComponent {
  private modal: NgbModalRef;
  private okDialogProperties = new Properties();

  constructor(private modalService: NgbModal) {
    this.okDialogProperties.okCb = this.okCb;
  }

  open(content: any) {
    this.modal = this.modalService.open(content);
  }

  okCb = () => {
    this.modal.dismiss();
  }
}
