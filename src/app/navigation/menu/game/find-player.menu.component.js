"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Observable_1 = require("rxjs/Observable");
var core_1 = require("@angular/core");
var FindPlayerMenuComponent = (function () {
    function FindPlayerMenuComponent() {
    }
    FindPlayerMenuComponent.prototype.new = function () {
        console.log("trying to subscribe to ws");
        //let ws = new WebSocket("ws://127.0.0.1:91/api/test");
        Observable_1.Observable.webSocket("ws://localhost:8080/api/test").subscribe();
        //ws.send("Hello");
        // ws.getDataStream().subscribe(
        //   res => {
        //     var count = JSON.parse(res.data).value;
        //     console.log('Got: ' + count);
        //   },
        //   function(e) { console.log('Error: ' + e.message); },
        //   function() { console.log('Completed'); }
        // );
    };
    return FindPlayerMenuComponent;
}());
FindPlayerMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-findPlayer-menu]',
        template: "\n    <a class=\"dropdown-item\" href=\"#\" (click)=\"new()\">Find player</a>\n  "
    })
], FindPlayerMenuComponent);
exports.FindPlayerMenuComponent = FindPlayerMenuComponent;
//# sourceMappingURL=find-player.menu.component.js.map