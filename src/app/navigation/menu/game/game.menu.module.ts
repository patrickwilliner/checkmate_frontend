import { NgModule } from '@angular/core';

import { GameMenuComponent } from './game.menu.component';
import { NewMenuComponent } from './new.menu.component';
import { FindPlayerMenuComponent } from './find-player.menu.component';

@NgModule({
  declarations: [GameMenuComponent, NewMenuComponent, FindPlayerMenuComponent],
  exports: [GameMenuComponent]
})
export class GameMenuModule {
}
