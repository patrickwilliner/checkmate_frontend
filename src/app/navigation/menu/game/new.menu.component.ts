import { Component } from '@angular/core';

import { ChessboardService } from '../../../chessboard/chessboard.service';

@Component({
  selector: '[cm-new-menu]',
  template: `
    <a class="dropdown-item" href="#" (click)="new()">New <span style="float: right;">ctrl+n</span></a>
  `
})
export class NewMenuComponent {
  constructor(private chessboardService: ChessboardService) {
  }

  new() {
    this.chessboardService.newGame();
  }
}
