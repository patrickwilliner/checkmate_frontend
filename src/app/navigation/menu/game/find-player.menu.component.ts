import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';

@Component({
  selector: '[cm-findPlayer-menu]',
  template: `
    <a class="dropdown-item" href="#" (click)="new()">Find player</a>
  `
})
export class FindPlayerMenuComponent {
  new() {
    console.log("trying to subscribe to ws");
    //let ws = new WebSocket("ws://127.0.0.1:91/api/test");
    Observable.webSocket("ws://localhost:8080/api/test").subscribe();
    //ws.send("Hello");
    // ws.getDataStream().subscribe(
    //   res => {
    //     var count = JSON.parse(res.data).value;
    //     console.log('Got: ' + count);
    //   },
    //   function(e) { console.log('Error: ' + e.message); },
    //   function() { console.log('Completed'); }
    // );
  }
}
