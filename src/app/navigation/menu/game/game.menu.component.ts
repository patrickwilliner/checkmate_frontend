import { Component } from '@angular/core';

@Component({
  selector: '[cm-game-menu]',
  template: `
    <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Game
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <div cm-new-menu></div>
      <a class="dropdown-item disabled" href="#">Save</a>
      <a class="dropdown-item disabled" href="#">Load</a>
      <a cm-findPlayer-menu></a>
    </div>
  `
})
export class GameMenuComponent {
}
