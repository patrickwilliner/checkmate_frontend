"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GameMenuComponent = (function () {
    function GameMenuComponent() {
    }
    return GameMenuComponent;
}());
GameMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-game-menu]',
        template: "\n    <a class=\"nav-link dropdown-toggle\" href=\"http://example.com\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n      Game\n    </a>\n    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      <div cm-new-menu></div>\n      <a class=\"dropdown-item disabled\" href=\"#\">Save</a>\n      <a class=\"dropdown-item disabled\" href=\"#\">Load</a>\n      <a cm-findPlayer-menu></a>\n    </div>\n  "
    })
], GameMenuComponent);
exports.GameMenuComponent = GameMenuComponent;
//# sourceMappingURL=game.menu.component.js.map