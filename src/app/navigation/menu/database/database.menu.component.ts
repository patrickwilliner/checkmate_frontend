import { Component } from '@angular/core';

@Component({
  selector: '[cm-database-menu]',
  template: `
    <a class="nav-link dropdown-toggle disabled" href="" id="navbarDropdownMenuLink" data-toggle="dropdown">
      Database
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      
    </div>
  `
})
export class DatabaseMenuComponent {
}
