"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DatabaseMenuComponent = (function () {
    function DatabaseMenuComponent() {
    }
    return DatabaseMenuComponent;
}());
DatabaseMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-database-menu]',
        template: "\n    <a class=\"nav-link dropdown-toggle disabled\" href=\"\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\">\n      Database\n    </a>\n    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n      \n    </div>\n  "
    })
], DatabaseMenuComponent);
exports.DatabaseMenuComponent = DatabaseMenuComponent;
//# sourceMappingURL=database.menu.component.js.map