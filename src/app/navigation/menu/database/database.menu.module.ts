import { NgModule } from '@angular/core';

import { DatabaseMenuComponent } from './database.menu.component';

@NgModule({
  imports: [],
  declarations: [DatabaseMenuComponent],
  exports: [DatabaseMenuComponent]
})
export class DatabaseMenuModule {
}
