"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var session_service_1 = require("../../../session/session.service");
var yes_no_dialog_component_1 = require("../../../dialog/yes-no.dialog.component");
var LogoutMenuComponent = (function () {
    function LogoutMenuComponent(modalService, sessionService) {
        var _this = this;
        this.modalService = modalService;
        this.sessionService = sessionService;
        this.dialogProperties = new yes_no_dialog_component_1.Properties();
        this.logoutCb = function () {
            _this.sessionService.logout().subscribe(function (data) {
                _this.modal.dismiss();
            }, function (error) {
                // TODO
                //this.form.errorMessage = 'Unable to login.';
            });
        };
        this.dismissDialogCb = function () { };
        this.dialogProperties.yesCb = this.logoutCb;
        this.dialogProperties.noCb = this.dismissDialogCb;
    }
    LogoutMenuComponent.prototype.open = function (content) {
        this.modal = this.modalService.open(content);
    };
    return LogoutMenuComponent;
}());
LogoutMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-logout-menu]',
        template: "\n    <ng-template #content>\n      <div cm-yes-no-dialog title=\"Login\" [properties]=\"dialogProperties\">\n        <p>Are you sure?</p>\n      </div>\n    </ng-template>\n    <a class=\"dropdown-item\" href=\"#\" (click)=\"open(content)\">Logout</a>\n  "
    }),
    __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal, session_service_1.SessionService])
], LogoutMenuComponent);
exports.LogoutMenuComponent = LogoutMenuComponent;
//# sourceMappingURL=logout.menu.component.js.map