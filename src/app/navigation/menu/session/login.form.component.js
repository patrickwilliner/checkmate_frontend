"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var LoginFormComponent = (function () {
    function LoginFormComponent() {
    }
    return LoginFormComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Function)
], LoginFormComponent.prototype, "okCb", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", FormData)
], LoginFormComponent.prototype, "form", void 0);
LoginFormComponent = __decorate([
    core_1.Component({
        selector: '[cm-login-form]',
        template: "\n    <form>\n      <!-- input fields -->\n      <div class=\"form-group\">\n        <label class=\"col-form-label\" for=\"login\">Login</label>\n        <input class=\"form-control\" type=\"text\" id=\"login\" name=\"login\" [(ngModel)]=\"form.login\" placeholder=\"Enter login\" autofocus>\n      </div>\n      <div class=\"form-group\">\n        <label class=\"col-form-label\" for=\"password\">Password</label>\n        <input class=\"form-control\" type=\"password\" id=\"password\" name=\"password\" [(ngModel)]=\"form.password\" placeholder=\"Enter password\">\n      </div>\n    </form>\n  "
    })
], LoginFormComponent);
exports.LoginFormComponent = LoginFormComponent;
var FormData = (function () {
    function FormData() {
    }
    return FormData;
}());
exports.FormData = FormData;
//# sourceMappingURL=login.form.component.js.map