import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

import { DialogModule } from '../../../dialog/dialog.module';

import { SessionMenuComponent } from './session.menu.component';
import { LoginMenuComponent } from './login.menu.component';
import { RegisterMenuComponent } from './register.menu.component';
import { LogoutMenuComponent } from './logout.menu.component';

import { LoginFormComponent } from './login.form.component';
import { UserFormComponent } from './user.form.component';

@NgModule({
  imports: [CommonModule, FormsModule, NgbModule, DialogModule],
  declarations: [SessionMenuComponent, LoginMenuComponent, RegisterMenuComponent, LogoutMenuComponent, LoginFormComponent, UserFormComponent],
  exports: [SessionMenuComponent]
})
export class SessionMenuModule {
}
