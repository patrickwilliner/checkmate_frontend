import { Component } from '@angular/core';

import { SessionService } from '../../../session/session.service';
import { Session } from '../../../session/session.model';
import { User } from '..//../../session/user.model';

@Component({
  selector: '[cm-session-menu]',
  template: `
    <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown">User</a>
    <div class="dropdown-menu">
      <div cm-login-menu></div>
      <div cm-register-menu></div>
      <div cm-logout-menu></div>
    </div>
  `
})
export class SessionMenuComponent {
  private session: Session;

  constructor(private sessionService: SessionService) {
  }

  getMenuLabel() {
    return this.session ? this.session.login : 'Login';
  }
}
