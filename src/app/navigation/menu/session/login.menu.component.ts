import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../../../session/user.service';
import { SessionService } from '../../../session/session.service';
import { User } from '../../../session/user.model';
import { FormData } from './login.form.component';
import { Properties } from '../../../dialog/ok.dialog.component';

@Component({
  selector: '[cm-login-menu]',
  template: `
    <ng-template #content>
      <div cm-ok-dialog title="Login" okBtnLabel="Login" [form]="form" [properties]="okDialogProperties">
        <div cm-login-form [form]="form"></div>
      </div>
    </ng-template>
    <a class="dropdown-item" href="#" (click)="open(content)">Login</a>
  `
})
export class LoginMenuComponent {
  private modal: NgbModalRef;
  private form = new FormData();
  private okDialogProperties = new Properties();

  constructor(private modalService: NgbModal, private userService: UserService, private sessionService: SessionService) {
    this.okDialogProperties.okCb = this.authenticateCb;
  }

  open(content: any) {
    this.modal = this.modalService.open(content);
  }

  authenticateCb = () => {
    this.sessionService.authenticate(this.form.login, this.form.password).subscribe(data => {
      this.modal.dismiss();
    }, error => {
      this.form.errorMessage = 'Unable to login.';
    })
  }
}
