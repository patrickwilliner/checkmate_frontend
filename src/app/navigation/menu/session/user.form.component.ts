import { Component, Input } from '@angular/core';

import { User } from '../../../session/user.model';
import { IFormData } from '../../../util/form-data.model';

@Component({
  selector: '[cm-user-form]',
  template: `
    <form>
      <!-- input fields -->
      <div class="row">
        <div class="col-6">
          <div class="form-group">
            <label for="firstName">First name</label>
            <input type="text" class="form-control" id="firstName" name="firstName" [(ngModel)]="form.firstName" placeholder="Enter first name" autofocus>
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
            <label for="lastName">Last name</label>
            <input type="text" class="form-control" id="lastName" name="lastName" [(ngModel)]="form.lastName" placeholder="Enter last name">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="form-group">
            <label for="login">Nickname</label>
            <input type="text" class="form-control" id="nickname" name="nickname" [(ngModel)]="form.nickname" placeholder="Enter nickname">
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" [(ngModel)]="form.email" placeholder="Enter email">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" [(ngModel)]="form.password" placeholder="Enter password">
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
            <label for="passwordConfirmation">Confirm password</label>
            <input type="password" class="form-control" id="passwordConfirmation" name="passwordConfirmation" [(ngModel)]="form.passwordConfirmation" placeholder="Confirm password">
          </div>
        </div>
      </div>
    </form>
  `
})
export class UserFormComponent {
  @Input() private form: FormData;
}

export class FormData implements IFormData {
  firstName: string;
  lastName: string;
  nickname: string;
  email: string;
  password: string;
  passwordConfirmation: string;
  errorMessage: string;
}
