"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var UserFormComponent = (function () {
    function UserFormComponent() {
    }
    return UserFormComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", FormData)
], UserFormComponent.prototype, "form", void 0);
UserFormComponent = __decorate([
    core_1.Component({
        selector: '[cm-user-form]',
        template: "\n    <form>\n      <!-- input fields -->\n      <div class=\"row\">\n        <div class=\"col-6\">\n          <div class=\"form-group\">\n            <label for=\"firstName\">First name</label>\n            <input type=\"text\" class=\"form-control\" id=\"firstName\" name=\"firstName\" [(ngModel)]=\"form.firstName\" placeholder=\"Enter first name\" autofocus>\n          </div>\n        </div>\n        <div class=\"col-6\">\n          <div class=\"form-group\">\n            <label for=\"lastName\">Last name</label>\n            <input type=\"text\" class=\"form-control\" id=\"lastName\" name=\"lastName\" [(ngModel)]=\"form.lastName\" placeholder=\"Enter last name\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-6\">\n          <div class=\"form-group\">\n            <label for=\"login\">Nickname</label>\n            <input type=\"text\" class=\"form-control\" id=\"nickname\" name=\"nickname\" [(ngModel)]=\"form.nickname\" placeholder=\"Enter nickname\">\n          </div>\n        </div>\n        <div class=\"col-6\">\n          <div class=\"form-group\">\n            <label for=\"email\">Email</label>\n            <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"form.email\" placeholder=\"Enter email\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-6\">\n          <div class=\"form-group\">\n            <label for=\"password\">Password</label>\n            <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"form.password\" placeholder=\"Enter password\">\n          </div>\n        </div>\n        <div class=\"col-6\">\n          <div class=\"form-group\">\n            <label for=\"passwordConfirmation\">Confirm password</label>\n            <input type=\"password\" class=\"form-control\" id=\"passwordConfirmation\" name=\"passwordConfirmation\" [(ngModel)]=\"form.passwordConfirmation\" placeholder=\"Confirm password\">\n          </div>\n        </div>\n      </div>\n    </form>\n  "
    })
], UserFormComponent);
exports.UserFormComponent = UserFormComponent;
var FormData = (function () {
    function FormData() {
    }
    return FormData;
}());
exports.FormData = FormData;
//# sourceMappingURL=user.form.component.js.map