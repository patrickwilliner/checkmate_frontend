"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var user_service_1 = require("../../../session/user.service");
var user_form_component_1 = require("./user.form.component");
var ok_dialog_component_1 = require("../../../dialog/ok.dialog.component");
var RegisterMenuComponent = (function () {
    function RegisterMenuComponent(modalService, userService) {
        var _this = this;
        this.modalService = modalService;
        this.userService = userService;
        this.form = new user_form_component_1.FormData();
        this.okDialogProperties = new ok_dialog_component_1.Properties();
        this.registerCb = function () {
            _this.userService.createUser(_this.form).subscribe(function (res) {
                _this.modal.close();
            }, function (err) {
                _this.form.errorMessage = "Unable to register.";
            });
        };
        this.okDialogProperties.okCb = this.registerCb;
    }
    RegisterMenuComponent.prototype.open = function (content) {
        this.modal = this.modalService.open(content);
    };
    return RegisterMenuComponent;
}());
RegisterMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-register-menu]',
        template: "\n    <ng-template #content>\n      <div cm-ok-dialog title=\"Register user\" okBtnLabel=\"Register\" [form]=\"form\" [properties]=\"okDialogProperties\">\n        <div cm-user-form [form]=\"form\"></div>\n      </div>\n    </ng-template>\n    <a class=\"dropdown-item\" href=\"#\" (click)=\"open(content)\">Register...</a>\n  "
    }),
    __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal, user_service_1.UserService])
], RegisterMenuComponent);
exports.RegisterMenuComponent = RegisterMenuComponent;
//# sourceMappingURL=register.menu.component.js.map