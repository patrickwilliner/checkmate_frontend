"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var common_1 = require("@angular/common");
var dialog_module_1 = require("../../../dialog/dialog.module");
var session_menu_component_1 = require("./session.menu.component");
var login_menu_component_1 = require("./login.menu.component");
var register_menu_component_1 = require("./register.menu.component");
var logout_menu_component_1 = require("./logout.menu.component");
var login_form_component_1 = require("./login.form.component");
var user_form_component_1 = require("./user.form.component");
var SessionMenuModule = (function () {
    function SessionMenuModule() {
    }
    return SessionMenuModule;
}());
SessionMenuModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule, forms_1.FormsModule, ng_bootstrap_1.NgbModule, dialog_module_1.DialogModule],
        declarations: [session_menu_component_1.SessionMenuComponent, login_menu_component_1.LoginMenuComponent, register_menu_component_1.RegisterMenuComponent, logout_menu_component_1.LogoutMenuComponent, login_form_component_1.LoginFormComponent, user_form_component_1.UserFormComponent],
        exports: [session_menu_component_1.SessionMenuComponent]
    })
], SessionMenuModule);
exports.SessionMenuModule = SessionMenuModule;
//# sourceMappingURL=session.menu.module.js.map