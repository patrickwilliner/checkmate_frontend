"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var user_service_1 = require("../../../session/user.service");
var session_service_1 = require("../../../session/session.service");
var login_form_component_1 = require("./login.form.component");
var ok_dialog_component_1 = require("../../../dialog/ok.dialog.component");
var LoginMenuComponent = (function () {
    function LoginMenuComponent(modalService, userService, sessionService) {
        var _this = this;
        this.modalService = modalService;
        this.userService = userService;
        this.sessionService = sessionService;
        this.form = new login_form_component_1.FormData();
        this.okDialogProperties = new ok_dialog_component_1.Properties();
        this.authenticateCb = function () {
            _this.sessionService.authenticate(_this.form.login, _this.form.password).subscribe(function (data) {
                _this.modal.dismiss();
            }, function (error) {
                _this.form.errorMessage = 'Unable to login.';
            });
        };
        this.okDialogProperties.okCb = this.authenticateCb;
    }
    LoginMenuComponent.prototype.open = function (content) {
        this.modal = this.modalService.open(content);
    };
    return LoginMenuComponent;
}());
LoginMenuComponent = __decorate([
    core_1.Component({
        selector: '[cm-login-menu]',
        template: "\n    <ng-template #content>\n      <div cm-ok-dialog title=\"Login\" okBtnLabel=\"Login\" [form]=\"form\" [properties]=\"okDialogProperties\">\n        <div cm-login-form [form]=\"form\"></div>\n      </div>\n    </ng-template>\n    <a class=\"dropdown-item\" href=\"#\" (click)=\"open(content)\">Login</a>\n  "
    }),
    __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal, user_service_1.UserService, session_service_1.SessionService])
], LoginMenuComponent);
exports.LoginMenuComponent = LoginMenuComponent;
//# sourceMappingURL=login.menu.component.js.map