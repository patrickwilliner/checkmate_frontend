import { Component } from '@angular/core';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { User } from '../../../session/user.model';
import { UserService } from '../../../session/user.service';
import { FormData } from './user.form.component';
import { Properties } from '../../../dialog/ok.dialog.component';

@Component({
  selector: '[cm-register-menu]',
  template: `
    <ng-template #content>
      <div cm-ok-dialog title="Register user" okBtnLabel="Register" [form]="form" [properties]="okDialogProperties">
        <div cm-user-form [form]="form"></div>
      </div>
    </ng-template>
    <a class="dropdown-item" href="#" (click)="open(content)">Register...</a>
  `
})
export class RegisterMenuComponent {
  private form = new FormData();
  private okDialogProperties = new Properties();

  private modal: NgbModalRef;

  constructor(private modalService: NgbModal, private userService: UserService) {
    this.okDialogProperties.okCb = this.registerCb;
  }

  open(content: any) {
    this.modal = this.modalService.open(content);
  }

  registerCb = () => {
    this.userService.createUser(this.form).subscribe((res: any) => {
      this.modal.close();
    }, (err: any) => {
      this.form.errorMessage = "Unable to register.";
    });
  }
}
