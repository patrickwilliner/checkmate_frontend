import { Component } from '@angular/core';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { SessionService } from '../../../session/session.service';
import { Properties } from '../../../dialog/yes-no.dialog.component';

@Component({
  selector: '[cm-logout-menu]',
  template: `
    <ng-template #content>
      <div cm-yes-no-dialog title="Login" [properties]="dialogProperties">
        <p>Are you sure?</p>
      </div>
    </ng-template>
    <a class="dropdown-item" href="#" (click)="open(content)">Logout</a>
  `
})
export class LogoutMenuComponent {
  private modal: NgbModalRef;
  private dialogProperties = new Properties();

  constructor(private modalService: NgbModal, private sessionService: SessionService) {
    this.dialogProperties.yesCb = this.logoutCb;
    this.dialogProperties.noCb = this.dismissDialogCb;
  }

  open(content: any) {
    this.modal = this.modalService.open(content);
  }

  logoutCb = () => {
    this.sessionService.logout().subscribe(data => {
      this.modal.dismiss();
    }, error => {
      // TODO
      //this.form.errorMessage = 'Unable to login.';
    })
  }

  dismissDialogCb = () => {}
}
