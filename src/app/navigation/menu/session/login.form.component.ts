import { Component, Input } from '@angular/core';

import { User } from '../../../session/user.model';
import { IFormData } from '../../../util/form-data.model';

@Component({
  selector: '[cm-login-form]',
  template: `
    <form>
      <!-- input fields -->
      <div class="form-group">
        <label class="col-form-label" for="login">Login</label>
        <input class="form-control" type="text" id="login" name="login" [(ngModel)]="form.login" placeholder="Enter login" autofocus>
      </div>
      <div class="form-group">
        <label class="col-form-label" for="password">Password</label>
        <input class="form-control" type="password" id="password" name="password" [(ngModel)]="form.password" placeholder="Enter password">
      </div>
    </form>
  `
})
export class LoginFormComponent {
  @Input() private okCb: (event: any) => void;
  @Input() private form: FormData;
}

export class FormData implements IFormData {
  login: string;
  password: string;
  errorMessage: string;
}
