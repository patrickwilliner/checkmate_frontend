"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var session_module_1 = require("../session/session.module");
var game_menu_module_1 = require("./menu/game/game.menu.module");
var statistics_menu_module_1 = require("./menu/statistics/statistics.menu.module");
var database_menu_module_1 = require("./menu/database/database.menu.module");
var session_menu_module_1 = require("./menu/session/session.menu.module");
var help_menu_module_1 = require("./menu/help/help.menu.module");
var navigation_bar_component_1 = require("./navigation-bar.component");
var session_user_component_1 = require("./session-user.component");
var NavigationModule = (function () {
    function NavigationModule() {
    }
    return NavigationModule;
}());
NavigationModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule, session_module_1.SessionModule, game_menu_module_1.GameMenuModule, statistics_menu_module_1.StatisticsMenuModule, session_menu_module_1.SessionMenuModule, help_menu_module_1.HelpMenuModule, database_menu_module_1.DatabaseMenuModule],
        declarations: [navigation_bar_component_1.NavigationBarComponent, session_user_component_1.SessionUserComponent],
        exports: [navigation_bar_component_1.NavigationBarComponent]
    })
], NavigationModule);
exports.NavigationModule = NavigationModule;
//# sourceMappingURL=navigation.module.js.map