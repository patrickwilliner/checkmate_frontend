"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var NavigationBarComponent = (function () {
    function NavigationBarComponent() {
    }
    return NavigationBarComponent;
}());
NavigationBarComponent = __decorate([
    core_1.Component({
        selector: '[cm-navigation-bar]',
        template: "\n    <nav class=\"navbar navbar-toggleable-md navbar-light bg-faded\">\n      <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n      </button>\n      <a class=\"navbar-brand\" href=\"#\">Check Mate</a>\n\n      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n        <ul class=\"navbar-nav mr-auto\">\n          <li class=\"nav-item dropdown\" cm-game-menu></li>\n          <li class=\"nav-item dropdown\" cm-statistics-menu></li>\n          <li class=\"nav-item dropdown\" cm-database-menu></li>\n          <li class=\"nav-item dropdown\" cm-session-menu></li>\n          <li class=\"nav-item dropdown\" cm-help-menu></li>\n        </ul>\n      </div>\n\n      <div class=\"navbar-text\" cm-session-user></div>\n    </nav>\n  "
    })
], NavigationBarComponent);
exports.NavigationBarComponent = NavigationBarComponent;
//# sourceMappingURL=navigation-bar.component.js.map