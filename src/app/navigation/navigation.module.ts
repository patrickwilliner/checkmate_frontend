import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionModule } from '../session/session.module';

import { GameMenuModule } from './menu/game/game.menu.module';
import { StatisticsMenuModule } from './menu/statistics/statistics.menu.module';
import { DatabaseMenuModule } from './menu/database/database.menu.module';
import { SessionMenuModule } from './menu/session/session.menu.module';
import { HelpMenuModule } from './menu/help/help.menu.module';

import { NavigationBarComponent } from './navigation-bar.component';
import { SessionUserComponent } from './session-user.component';

@NgModule({
  imports: [CommonModule, SessionModule, GameMenuModule, StatisticsMenuModule, SessionMenuModule, HelpMenuModule, DatabaseMenuModule],
  declarations: [NavigationBarComponent, SessionUserComponent],
  exports: [NavigationBarComponent]
})
export class NavigationModule {
}
