"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var notification_service_1 = require("../util/notification.service");
var session_service_1 = require("../session/session.service");
var SessionUserComponent = (function () {
    function SessionUserComponent(notificationService, sessionService) {
        this.notificationService = notificationService;
        this.sessionService = sessionService;
        this.vm = new Vm();
        notificationService.addListener('session:changed', this);
    }
    SessionUserComponent.prototype.notify = function () {
        this.vm.user = this.sessionService.sessionUser;
    };
    return SessionUserComponent;
}());
SessionUserComponent = __decorate([
    core_1.Component({
        selector: '[cm-session-user]',
        template: "\n    <span class=\"fa fa-user\" *ngIf=\"vm.user\">&nbsp;{{vm.user.nickname}}</span>\n  "
    }),
    __metadata("design:paramtypes", [notification_service_1.NotificationService, session_service_1.SessionService])
], SessionUserComponent);
exports.SessionUserComponent = SessionUserComponent;
var Vm = (function () {
    function Vm() {
    }
    return Vm;
}());
//# sourceMappingURL=session-user.component.js.map