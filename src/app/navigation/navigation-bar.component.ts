import { Component } from '@angular/core';

@Component({
  selector: '[cm-navigation-bar]',
  template: `
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">Check Mate</a>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown" cm-game-menu></li>
          <li class="nav-item dropdown" cm-statistics-menu></li>
          <li class="nav-item dropdown" cm-database-menu></li>
          <li class="nav-item dropdown" cm-session-menu></li>
          <li class="nav-item dropdown" cm-help-menu></li>
        </ul>
      </div>

      <div class="navbar-text" cm-session-user></div>
    </nav>
  `
})
export class NavigationBarComponent {
}
