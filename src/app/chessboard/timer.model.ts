import { Observable, Subject } from 'rxjs/Rx';

export class Timer {
  private startTime: Date;
  private stopTime: Date;
  private timer: Observable<number>;

  public subject = new Subject<number>();

  start() {
    this.startTime = new Date();
    delete this.stopTime;

    this.timer = Observable.timer(0, 1000);
    this.timer.subscribe(() => this.subject.next(this.getTotalTimeInSeconds()));

    return this;
  }

  stop() {
    this.stopTime = new Date();
    return this;
  }

  getTotalTimeInSeconds() {
    if (this.stopTime) {
      Math.floor((new Date().getTime() - this.startTime.getTime()) / 1000);
    } else if (this.startTime) {
      return Math.floor((new Date().getTime() - this.startTime.getTime()) / 1000);
    } else {
      return 0;
    }
  }
}
