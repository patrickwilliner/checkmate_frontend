import { Component, ViewChild, AfterViewInit } from '@angular/core';

import { Row } from './row.model';
import { Field } from './field.model';
import { Color } from './color.model';
import { Chessboard } from './chessboard.model';
import { Position } from './position.model';
import { ChessboardService } from './chessboard.service';
import { PawnPromotionDialogComponent } from './pawn-promotion.dialog.component';

import { LoggerService } from '../util/logger.service';

@Component({
  selector: '[cm-chessboard]',
  template: `
    <div cm-pawn-promotion-dialog></div>

    <div class="chessboard">
      <div class="row label">
        <div class="field label"></div>
        <div class="field">A</div>
        <div class="field">B</div>
        <div class="field">C</div>
        <div class="field">D</div>
        <div class="field">E</div>
        <div class="field">F</div>
        <div class="field">G</div>
        <div class="field">H</div>
      </div>

      <div class="row" *ngFor="let row of chessboardService.getChessboard().rows; let i = index;">
        <div class="field label"><div>{{8 - i}}</div></div>
        <div cm-field *ngFor="let field of row.fields" [field]="field"></div>
      </div>
    </div>
  `
})
export class ChessboardComponent {
  @ViewChild(PawnPromotionDialogComponent)
  private pawnPromotionDialog: PawnPromotionDialogComponent;

  constructor(private chessboardService: ChessboardService, private log: LoggerService) {
  }
}
