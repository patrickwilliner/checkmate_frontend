import { Injectable, ElementRef } from '@angular/core';

import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { Chessboard } from './chessboard.model';
import { PawnPromotionDialogComponent } from './pawn-promotion.dialog.component';

@Injectable()
export class ChessboardService {
  private chessboard = new Chessboard(this);
  private pawnPromotionDialogComponent: ElementRef;
  
  constructor(private modalService: NgbModal) {
  }

  registerPawnPromotionDialog(pawnPromotionDialogComponent: ElementRef) {
    this.pawnPromotionDialogComponent = pawnPromotionDialogComponent;
  }

  newGame() {
    this.chessboard.init();
    this.chessboard.start();
  }

  getChessboard(): Chessboard {
    return this.chessboard;
  }

  showPawnPromotionDialog() {
    console.log('xxx');
    this.modalService.open(this.pawnPromotionDialogComponent);
  }
}
