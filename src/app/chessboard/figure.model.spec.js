"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var figure_model_1 = require("./figure.model");
var color_model_1 = require("./color.model");
var chessboard_model_1 = require("./chessboard.model");
//import { ChessboardService } from './chessboard.service';
describe('Test <Figure> model', function () {
    // let chessboardServiceMock = new (class Mock extends ChessboardService {
    //     showPawnPromotionDialog() {
    //     }
    // })(null);
    var createChessboard = function () {
        return new chessboard_model_1.Chessboard(null);
    };
    it('Should set correct figures at init', function () {
        var chessboard = new chessboard_model_1.Chessboard(null);
        chessboard.init();
        // row 0
        [figure_model_1.Rook, figure_model_1.Knight, figure_model_1.Bishop, figure_model_1.Queen, figure_model_1.King, figure_model_1.Bishop, figure_model_1.Knight, figure_model_1.Rook].forEach(function (expectedFigure, i) {
            var figure = chessboard.getField({ x: i, y: 0 }).figure;
            expect(figure.constructor).toBe(expectedFigure);
            expect(figure.color).toBe(color_model_1.Color.White);
        });
        // row 1
        for (var i = 0; i < 8; i++) {
            var figure = chessboard.getField({ x: i, y: 1 }).figure;
            expect(figure.constructor).toBe(figure_model_1.Pawn);
            expect(figure.color).toBe(color_model_1.Color.White);
        }
        // row 7
        [figure_model_1.Rook, figure_model_1.Knight, figure_model_1.Bishop, figure_model_1.Queen, figure_model_1.King, figure_model_1.Bishop, figure_model_1.Knight, figure_model_1.Rook].forEach(function (expectedFigure, i) {
            var figure = chessboard.getField({ x: i, y: 7 }).figure;
            expect(figure.constructor).toBe(expectedFigure);
            expect(figure.color).toBe(color_model_1.Color.Black);
        });
        // row 6
        for (var i = 0; i < 8; i++) {
            var figure = chessboard.getField({ x: i, y: 6 }).figure;
            expect(figure.constructor).toBe(figure_model_1.Pawn);
            expect(figure.color).toBe(color_model_1.Color.Black);
        }
    });
    describe('Test pawn moves', function () {
        it('pawn moves one field forward', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            chessboard.setFigure({ x: 6, y: 1 }, whitePawn);
            chessboard.move({ x: 6, y: 1 }, { x: 6, y: 2 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBeUndefined();
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(whitePawn);
        });
        it('pawn moves two fields forward', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            chessboard.setFigure({ x: 6, y: 1 }, whitePawn);
            chessboard.move({ x: 6, y: 1 }, { x: 6, y: 3 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBeUndefined();
            expect(chessboard.getField({ x: 6, y: 3 }).figure).toBe(whitePawn);
        });
        it('pawn does not move backward', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            chessboard.setFigure({ x: 6, y: 3 }, whitePawn);
            chessboard.move({ x: 6, y: 3 }, { x: 6, y: 2 });
            expect(chessboard.getField({ x: 6, y: 3 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBeUndefined();
        });
        it('pawn does not move diagonal', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            chessboard.setFigure({ x: 6, y: 1 }, whitePawn);
            chessboard.move({ x: 6, y: 1 }, { x: 5, y: 2 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 5, y: 2 }).figure).toBeUndefined();
            chessboard.move({ x: 6, y: 1 }, { x: 7, y: 2 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 7, y: 2 }).figure).toBeUndefined();
        });
        it('pawn does not move horizontal', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            chessboard.setFigure({ x: 6, y: 1 }, whitePawn);
            chessboard.move({ x: 6, y: 1 }, { x: 5, y: 1 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 5, y: 1 }).figure).toBeUndefined();
            chessboard.move({ x: 6, y: 1 }, { x: 7, y: 1 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 7, y: 1 }).figure).toBeUndefined();
        });
        it('pawn does not move more than one field', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            chessboard.setFigure({ x: 6, y: 2 }, whitePawn);
            chessboard.move({ x: 6, y: 2 }, { x: 6, y: 4 });
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 5, y: 4 }).figure).toBeUndefined();
            chessboard.move({ x: 6, y: 2 }, { x: 6, y: 5 });
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 5, y: 5 }).figure).toBeUndefined();
            chessboard.move({ x: 6, y: 2 }, { x: 6, y: 6 });
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 5, y: 6 }).figure).toBeUndefined();
            chessboard.move({ x: 6, y: 2 }, { x: 6, y: 7 });
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 5, y: 7 }).figure).toBeUndefined();
        });
        it('blocked pawn cannot move', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            var blackPawn = new figure_model_1.Pawn(color_model_1.Color.Black);
            chessboard.setFigure({ x: 6, y: 1 }, whitePawn);
            chessboard.setFigure({ x: 6, y: 2 }, blackPawn);
            chessboard.move({ x: 6, y: 1 }, { x: 6, y: 2 });
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBe(whitePawn);
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(blackPawn);
            chessboard.move({ x: 6, y: 1 }, { x: 6, y: 3 });
            expect(chessboard.getField({ x: 6, y: 2 }).figure).toBe(blackPawn);
            expect(chessboard.getField({ x: 6, y: 3 }).figure).toBeUndefined();
        });
        xit('pawn beats other figure', function () {
            var chessboard = createChessboard();
            var whitePawn = new figure_model_1.Pawn(color_model_1.Color.White);
            var blackPawn = new figure_model_1.Pawn(color_model_1.Color.Black);
            chessboard.setFigure({ x: 6, y: 1 }, whitePawn);
            chessboard.setFigure({ x: 5, y: 2 }, blackPawn);
            chessboard.move({ x: 6, y: 1 }, { x: 5, y: 2 });
            expect(chessboard.getField({ x: 5, y: 2 }).figure).toBe(blackPawn);
            expect(chessboard.getField({ x: 6, y: 1 }).figure).toBeUndefined();
        });
    });
    describe('Test bishop moves', function () {
        it('bishop moves diagonally', function () {
            var chessboard = createChessboard();
            var whiteBishop = new figure_model_1.Bishop(color_model_1.Color.White);
            chessboard.setFigure({ x: 5, y: 0 }, whiteBishop);
            chessboard.move({ x: 5, y: 0 }, { x: 4, y: 1 });
            expect(chessboard.getField({ x: 4, y: 1 }).figure).toBe(whiteBishop);
            expect(chessboard.getField({ x: 5, y: 0 }).figure).toBeUndefined();
            chessboard.activeColor = color_model_1.Color.White;
            chessboard.move({ x: 4, y: 1 }, { x: 6, y: 3 });
            expect(chessboard.getField({ x: 6, y: 3 }).figure).toBe(whiteBishop);
            expect(chessboard.getField({ x: 4, y: 1 }).figure).toBeUndefined();
        });
        xit('blocked bishop cannot move', function () {
            var chessboard = createChessboard();
            var whiteBishop = new figure_model_1.Bishop(color_model_1.Color.White);
            var blackPawn = new figure_model_1.Bishop(color_model_1.Color.Black);
            chessboard.setFigure({ x: 5, y: 0 }, whiteBishop);
            chessboard.setFigure({ x: 4, y: 1 }, blackPawn);
            chessboard.move({ x: 5, y: 0 }, { x: 3, y: 2 });
            expect(chessboard.getField({ x: 5, y: 0 }).figure).toBe(whiteBishop);
            expect(chessboard.getField({ x: 4, y: 1 }).figure).toBe(blackPawn);
            expect(chessboard.getField({ x: 3, y: 2 }).figure).toBeUndefined();
            chessboard.move({ x: 5, y: 0 }, { x: 2, y: 3 });
            expect(chessboard.getField({ x: 5, y: 0 }).figure).toBe(whiteBishop);
            expect(chessboard.getField({ x: 4, y: 1 }).figure).toBe(blackPawn);
            expect(chessboard.getField({ x: 2, y: 3 }).figure).toBeUndefined();
        });
    });
    describe('Test knight moves', function () {
    });
    describe('Test rook moves', function () {
    });
    describe('Test king moves', function () {
        it('king plays queenside castling', function () {
            var chessboard = createChessboard();
            var whiteKing = new figure_model_1.King(color_model_1.Color.White);
            var whiteRook = new figure_model_1.Rook(color_model_1.Color.White);
            chessboard.setFigure({ x: 4, y: 0 }, whiteKing);
            chessboard.setFigure({ x: 0, y: 0 }, whiteRook);
            chessboard.move({ x: 4, y: 0 }, { x: 2, y: 0 });
            expect(chessboard.getField({ x: 2, y: 0 }).figure).toBe(whiteKing);
        });
        it('king plays kingside castling', function () {
            var chessboard = createChessboard();
            var whiteKing = new figure_model_1.King(color_model_1.Color.White);
            var whiteRook = new figure_model_1.Rook(color_model_1.Color.White);
            chessboard.setFigure({ x: 4, y: 0 }, whiteKing);
            chessboard.setFigure({ x: 7, y: 0 }, whiteRook);
            chessboard.move({ x: 4, y: 0 }, { x: 6, y: 0 });
            expect(chessboard.getField({ x: 6, y: 0 }).figure).toBe(whiteKing);
        });
    });
    describe('Test queen moves', function () {
    });
});
//# sourceMappingURL=figure.model.spec.js.map