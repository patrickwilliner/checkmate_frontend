export class Color {
  public static Black: Color = new Color('black', 'black');
  public static White: Color = new Color('white', 'white');

  private constructor(public name: string, public cssClass: string) {
  }

  public getOther(): Color {
    return this === Color.Black ? Color.White : Color.Black;
  }
}
