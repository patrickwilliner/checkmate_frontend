import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ChessboardService } from './chessboard.service';

@Component({
  selector: '[cm-pawn-promotion-dialog]',
  template: `
    <ng-template #template>
      <div ok-dialog title="Select" okBtnLabel="oKiDoKi">
        <div class="row">
          <div class="col-3"><img src="res/img/chess_qlt45.svg"></div>
          <div class="col-3"><img src="res/img/chess_nlt45.svg"></div>
          <div class="col-3"><img src="res/img/chess_blt45.svg"></div>
          <div class="col-3"><img src="res/img/chess_rlt45.svg"></div>
        </div>
      </div>
    </ng-template>
  `
})
export class PawnPromotionDialogComponent implements AfterViewInit {
  private modal: NgbModalRef;
  @ViewChild('template') templateElement: ElementRef;

  constructor(private chessboardService: ChessboardService) {
  }

  ngAfterViewInit() {
    this.chessboardService.registerPawnPromotionDialog(this.templateElement);
  }
}
