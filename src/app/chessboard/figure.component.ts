import { Component, Input } from '@angular/core';

import { Figure } from './figure.model';
import { Field } from './field.model';

@Component({
  selector: '[cm-figure]',
  template: `<img *ngIf="figure" [src]="figure.imgSrc" (dragstart)="onDragStart($event)">`
})
export class FigureComponent {
  @Input() figure: Figure;

  public onDragStart(event: any): void {
    event.dataTransfer.setData('data', this.figure.field.position.toString());
  }
}
