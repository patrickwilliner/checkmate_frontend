"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Color = (function () {
    function Color(name, cssClass) {
        this.name = name;
        this.cssClass = cssClass;
    }
    Color.prototype.getOther = function () {
        return this === Color.Black ? Color.White : Color.Black;
    };
    return Color;
}());
Color.Black = new Color('black', 'black');
Color.White = new Color('white', 'white');
exports.Color = Color;
//# sourceMappingURL=color.model.js.map