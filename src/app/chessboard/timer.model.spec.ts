import { Timer } from './timer.model';

describe('<Timer> model', () => {
    it('creates a timer without starting it', () => {
        let timer = new Timer();
        expect(timer.getTotalTimeInSeconds()).toBe(timer.getTotalTimeInSeconds());
    });

    it('starts the timer', () => {
        let timer = new Timer().start();
        let firstTimeMeasure = timer.getTotalTimeInSeconds();
        setTimeout(() => {
            expect(timer.getTotalTimeInSeconds()).not.toBe(firstTimeMeasure);
        }, 1000);
    })

    it('stops the timer', () => {
        let timer = new Timer().start().stop();
        let firstTimeMeasure = timer.getTotalTimeInSeconds();
        setTimeout(() => {
            expect(timer.getTotalTimeInSeconds()).toBe(firstTimeMeasure);
        }, 1000);
    })
});