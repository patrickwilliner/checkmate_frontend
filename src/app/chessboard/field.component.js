"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var field_model_1 = require("./field.model");
var position_model_1 = require("./position.model");
var FieldComponent = (function () {
    function FieldComponent() {
        this.validMoveClass = '';
    }
    FieldComponent.prototype.onDrop = function (event) {
        var data = event.dataTransfer.getData('data');
        this.validMoveClass = '';
        this.field.chessboard.move(position_model_1.Position.fromString(data), this.field.position);
        event.preventDefault();
    };
    FieldComponent.prototype.onDragOver = function (event) {
        this.validMoveClass = 'validMove';
        event.preventDefault();
    };
    FieldComponent.prototype.onDragLeave = function (event) {
        this.validMoveClass = '';
        event.preventDefault();
    };
    return FieldComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", field_model_1.Field)
], FieldComponent.prototype, "field", void 0);
FieldComponent = __decorate([
    core_1.Component({
        selector: '[cm-field]',
        template: "\n    <div class=\"field {{field.color.cssClass}} {{validMoveClass}}\" (drop)=\"onDrop($event)\" (dragover)=\"onDragOver($event)\" (dragleave)=\"onDragLeave($event)\">\n      <div cm-figure [figure]=\"field.figure\"></div>\n    </div>\n  "
    })
], FieldComponent);
exports.FieldComponent = FieldComponent;
//# sourceMappingURL=field.component.js.map