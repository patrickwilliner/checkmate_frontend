"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var move_model_1 = require("./move.model");
var figure_model_1 = require("./figure.model");
var color_model_1 = require("./color.model");
describe('<Move> model', function () {
    it('creates string for kingside castling', function () {
        var king = new figure_model_1.King(color_model_1.Color.White);
        var move = move_model_1.Move.createKingsideCastling(king);
        expect(move.toString()).toBe('0-0');
    });
    it('creates string for queenside castling', function () {
        var king = new figure_model_1.King(color_model_1.Color.White);
        var move = move_model_1.Move.createQueensideCastling(king);
        expect(move.toString()).toBe('0-0-0');
    });
    it('creates string for pawn move', function () {
        var pawn = new figure_model_1.Pawn(color_model_1.Color.White);
        var move = move_model_1.Move.createSimpleMove(pawn, { x: 0, y: 6 }, { x: 0, y: 5 });
        expect(move.toString()).toBe('a3');
    });
    it('creates string for pawn beating move', function () {
        var pawn = new figure_model_1.Pawn(color_model_1.Color.White);
        var move = move_model_1.Move.createBeatingMove(pawn, { x: 0, y: 6 }, { x: 1, y: 5 });
        expect(move.toString()).toBe('axb3');
    });
    it('creates string for bishop move', function () {
        var pawn = new figure_model_1.Bishop(color_model_1.Color.White);
        var move = move_model_1.Move.createSimpleMove(pawn, { x: 0, y: 0 }, { x: 7, y: 7 });
        expect(move.toString()).toBe('Bh1');
    });
    it('creates string for queen beating move', function () {
        var pawn = new figure_model_1.Queen(color_model_1.Color.White);
        var move = move_model_1.Move.createBeatingMove(pawn, { x: 0, y: 0 }, { x: 7, y: 7 });
        expect(move.toString()).toBe('Qxh1');
    });
});
//# sourceMappingURL=move.model.spec.js.map