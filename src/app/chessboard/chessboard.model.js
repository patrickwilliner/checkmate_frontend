"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var row_model_1 = require("./row.model");
var figure_model_1 = require("./figure.model");
var position_model_1 = require("./position.model");
var field_model_1 = require("./field.model");
var color_model_1 = require("./color.model");
var timer_model_1 = require("./timer.model");
var move_model_1 = require("./move.model");
var _ = require("lodash");
var Chessboard = (function () {
    function Chessboard(chessboardService) {
        this.chessboardService = chessboardService;
        this.rows = [];
        this.activeColor = color_model_1.Color.White;
        this.timer = new timer_model_1.Timer();
        this.history = [];
        this.buildChessboard();
    }
    Chessboard.prototype.start = function () {
        this.timer.start();
    };
    Chessboard.prototype.setFigure = function (position, figure) {
        var field = this.getField(position);
        if (figure) {
            figure.field = field;
        }
        field.figure = figure;
    };
    Chessboard.prototype.getField = function (position) {
        return this.rows[position.y].fields[position.x];
    };
    Chessboard.prototype.move = function (from, to) {
        var figure = this.getField(from).figure;
        if (figure instanceof figure_model_1.King) {
            if (figure.isKingsideCastling(from, to)) {
                this.setFigure(new position_model_1.Position(5, from.y), this.getField(new position_model_1.Position(7, from.y)).figure);
                this.setFigure(new position_model_1.Position(7, from.y), undefined);
                this.setFigure(to, figure);
                this.setFigure(from, undefined);
                this.activeColor = this.activeColor.getOther();
                this.history.push(move_model_1.Move.createKingsideCastling(figure));
                figure.setHasMoved();
                return;
            }
            else if (figure.isQueensideCastling(from, to)) {
                this.setFigure(new position_model_1.Position(3, from.y), this.getField(new position_model_1.Position(0, from.y)).figure);
                this.setFigure(new position_model_1.Position(0, from.y), undefined);
                this.setFigure(to, figure);
                this.setFigure(from, undefined);
                this.activeColor = this.activeColor.getOther();
                this.history.push(move_model_1.Move.createQueensideCastling(figure));
                figure.setHasMoved();
                return;
            }
        }
        if (this.isBeatingMove(from, to)) {
            this.setFigure(to, figure);
            this.setFigure(from, undefined);
            this.activeColor = this.activeColor.getOther();
            this.history.push(new move_model_1.Move(figure, from, to, true));
            figure.setHasMoved();
            this.checkAndHandlePawnPromotion(figure, from, to);
        }
        else if (this.isValidMove(from, to)) {
            this.setFigure(to, figure);
            this.setFigure(from, undefined);
            this.activeColor = this.activeColor.getOther();
            this.history.push(new move_model_1.Move(figure, from, to));
            figure.setHasMoved();
            this.checkAndHandlePawnPromotion(figure, from, to);
        }
    };
    Chessboard.prototype.checkAndHandlePawnPromotion = function (figure, from, to) {
        if ((figure instanceof figure_model_1.Pawn) && figure.isPawnPromotion(from, to)) {
            this.chessboardService.showPawnPromotionDialog();
        }
    };
    Chessboard.prototype.isBeatingMove = function (from, to) {
        var figure = this.getField(from).figure;
        return figure.isBeatingMove(from, to);
    };
    Chessboard.prototype.isValidMove = function (from, to) {
        var figure = this.getField(from).figure;
        return figure.color === this.activeColor && figure.isValidMove(from, to);
    };
    Chessboard.prototype.init = function () {
        this.activeColor = color_model_1.Color.White;
        this.rows = [];
        this.buildChessboard();
        this.initFigures();
        this.history = [];
        this.timer = new timer_model_1.Timer();
    };
    Chessboard.prototype.initFigures = function () {
        this.initWhiteFigures();
        this.initBlackFigures();
    };
    Chessboard.prototype.toString = function () {
        var s = '\n  0 1 2 3 4 5 6 7\n -----------------\n';
        for (var i = 0; i < this.rows.length; i++) {
            var row = this.rows[i];
            s += i + '|' + _.map(row.fields, function (field) {
                if (field.figure && field.figure.color === color_model_1.Color.White) {
                    return (field.figure.shortName || 'p').toUpperCase();
                }
                else if (field.figure && field.figure.color === color_model_1.Color.Black) {
                    return (field.figure.shortName || 'p').toLowerCase();
                }
                else {
                    return ' ';
                }
            }).join('|') + '|\n';
        }
        return s;
    };
    Chessboard.prototype.buildChessboard = function () {
        // TODO turn board (starting field color should be white and black plays on top)
        var currentColor = color_model_1.Color.White;
        for (var y = 0; y < 8; y++) {
            var row = new row_model_1.Row();
            this.rows.push(row);
            currentColor = currentColor.getOther();
            for (var x = 0; x < 8; x++) {
                row.fields.push(new field_model_1.Field(new position_model_1.Position(x, y), currentColor, this));
                currentColor = currentColor.getOther();
            }
        }
    };
    Chessboard.prototype.initWhiteFigures = function () {
        this.setFigure(new position_model_1.Position(0, 0), new figure_model_1.Rook(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(1, 0), new figure_model_1.Knight(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(2, 0), new figure_model_1.Bishop(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(3, 0), new figure_model_1.Queen(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(4, 0), new figure_model_1.King(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(5, 0), new figure_model_1.Bishop(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(6, 0), new figure_model_1.Knight(color_model_1.Color.White));
        this.setFigure(new position_model_1.Position(7, 0), new figure_model_1.Rook(color_model_1.Color.White));
        for (var i = 0; i < 8; i++) {
            this.setFigure(new position_model_1.Position(i, 1), new figure_model_1.Pawn(color_model_1.Color.White));
        }
    };
    Chessboard.prototype.initBlackFigures = function () {
        for (var i = 0; i < 8; i++) {
            this.setFigure(new position_model_1.Position(i, 6), new figure_model_1.Pawn(color_model_1.Color.Black));
        }
        this.setFigure(new position_model_1.Position(0, 7), new figure_model_1.Rook(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(1, 7), new figure_model_1.Knight(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(2, 7), new figure_model_1.Bishop(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(3, 7), new figure_model_1.Queen(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(4, 7), new figure_model_1.King(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(5, 7), new figure_model_1.Bishop(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(6, 7), new figure_model_1.Knight(color_model_1.Color.Black));
        this.setFigure(new position_model_1.Position(7, 7), new figure_model_1.Rook(color_model_1.Color.Black));
    };
    return Chessboard;
}());
exports.Chessboard = Chessboard;
//# sourceMappingURL=chessboard.model.js.map