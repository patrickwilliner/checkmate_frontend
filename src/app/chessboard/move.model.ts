import { Figure, Pawn } from './figure.model';
import { Position } from './position.model';

export class Move {
  isCheck: boolean;
  isPawnPromotion: boolean;
  isKingsideCastling: boolean;
  isQueensideCastling: boolean;

  constructor(public figure: Figure, public from?: Position, public to?: Position, public beat?: boolean) {
  }

  toString() {
    if (this.isKingsideCastling) {
      return '0-0';
    } else if (this.isQueensideCastling) {
      return '0-0-0';
    } else if (this.beat && (this.figure instanceof Pawn)) {
      return String.fromCharCode(97 + this.from.x) + 'x' + String.fromCharCode(97 + this.to.x) + (8 - this.to.y);
    } else if (this.beat) {
      return this.figure.shortName.toUpperCase() + 'x' + String.fromCharCode(97 + this.to.x) + (8 - this.to.y);
    } else {
      return this.figure.shortName.toUpperCase() + String.fromCharCode(97 + this.to.x) + (8 - this.to.y);
    }
  }

  static createSimpleMove(figure: Figure, from: Position, to: Position) {
    let move = new Move(figure);
    move.from = from;
    move.to = to;
    return move;
  }

  static createBeatingMove(figure: Figure, from: Position, to: Position) {
    let move = new Move(figure);
    move.from = from;
    move.to = to;
    move.beat = true;
    return move;
  }

  static createKingsideCastling(figure: Figure) {
    let move = new Move(figure);
    move.isKingsideCastling = true;
    return move;
  }

  static createQueensideCastling(figure: Figure) {
    let move = new Move(figure);
    move.isQueensideCastling = true;
    return move;
  }

  static createPawnPromotion(pawn: Pawn, from: Position, to: Position) {
    let move = new Move(pawn);
    move.isPawnPromotion = true;
    return move;
  }
}
