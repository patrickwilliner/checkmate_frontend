export class Position {
  constructor(public x?: number, public y?: number) {
    // TODO assert range
  }

  public toString(): string {
    return JSON.stringify(this);
  }

  public static fromString(value: string): Position {
    return JSON.parse(value);
  }
}
