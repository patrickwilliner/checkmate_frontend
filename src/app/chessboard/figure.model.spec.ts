import { Figure, Pawn, Rook, Bishop, Queen, King, Knight } from './figure.model';
import { Color } from './color.model';
import { Chessboard } from './chessboard.model';
//import { ChessboardService } from './chessboard.service';

describe('Test <Figure> model', () => {
    // let chessboardServiceMock = new (class Mock extends ChessboardService {
    //     showPawnPromotionDialog() {
    //     }
    // })(null);

    let createChessboard = () => {
        return new Chessboard(null);
    }

    it('Should set correct figures at init', () => {
        let chessboard = new Chessboard(null);
        chessboard.init();

        // row 0
        [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook].forEach((expectedFigure, i) => {
            let figure = chessboard.getField({x: i, y: 0}).figure;
            
            expect(figure.constructor).toBe(expectedFigure);
            expect(figure.color).toBe(Color.White);
        });
    
        // row 1
        for (let i = 0; i < 8; i++) {
            let figure = chessboard.getField({x: i, y: 1}).figure;
            
            expect(figure.constructor).toBe(Pawn);
            expect(figure.color).toBe(Color.White);
        }
        
        // row 7
        [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook].forEach((expectedFigure, i) => {
            let figure = chessboard.getField({x: i, y: 7}).figure;

            expect(figure.constructor).toBe(expectedFigure);
            expect(figure.color).toBe(Color.Black);
        });
        
        // row 6
        for (let i = 0; i < 8; i++) {
            let figure = chessboard.getField({x: i, y: 6}).figure;

            expect(figure.constructor).toBe(Pawn);
            expect(figure.color).toBe(Color.Black);
        }
    });
    
    describe('Test pawn moves', () => {
        it('pawn moves one field forward', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            chessboard.setFigure({x: 6, y: 1}, whitePawn);
            
            chessboard.move({x: 6, y: 1}, {x: 6, y: 2});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBeUndefined();
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(whitePawn);
        });

        it('pawn moves two fields forward', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            chessboard.setFigure({x: 6, y: 1}, whitePawn);
            
            chessboard.move({x: 6, y: 1}, {x: 6, y: 3});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBeUndefined();
            expect(chessboard.getField({x: 6, y: 3}).figure).toBe(whitePawn);
        });

        it('pawn does not move backward', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            chessboard.setFigure({x: 6, y: 3}, whitePawn);
            
            chessboard.move({x: 6, y: 3}, {x: 6, y: 2});
            expect(chessboard.getField({x: 6, y: 3}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 6, y: 2}).figure).toBeUndefined();
        });

        it('pawn does not move diagonal', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            chessboard.setFigure({x: 6, y: 1}, whitePawn);
            
            chessboard.move({x: 6, y: 1}, {x: 5, y: 2});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 5, y: 2}).figure).toBeUndefined();

            chessboard.move({x: 6, y: 1}, {x: 7, y: 2});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 7, y: 2}).figure).toBeUndefined();
        });

        it('pawn does not move horizontal', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            chessboard.setFigure({x: 6, y: 1}, whitePawn);
            
            chessboard.move({x: 6, y: 1}, {x: 5, y: 1});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 5, y: 1}).figure).toBeUndefined();

            chessboard.move({x: 6, y: 1}, {x: 7, y: 1});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 7, y: 1}).figure).toBeUndefined();
        });

        it('pawn does not move more than one field', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            chessboard.setFigure({x: 6, y: 2}, whitePawn);
            
            chessboard.move({x: 6, y: 2}, {x: 6, y: 4});
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 5, y: 4}).figure).toBeUndefined();

            chessboard.move({x: 6, y: 2}, {x: 6, y: 5});
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 5, y: 5}).figure).toBeUndefined();

            chessboard.move({x: 6, y: 2}, {x: 6, y: 6});
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 5, y: 6}).figure).toBeUndefined();

            chessboard.move({x: 6, y: 2}, {x: 6, y: 7});
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 5, y: 7}).figure).toBeUndefined();
        });

        it('blocked pawn cannot move', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            let blackPawn = new Pawn(Color.Black);

            chessboard.setFigure({x: 6, y: 1}, whitePawn);
            chessboard.setFigure({x: 6, y: 2}, blackPawn);
            
            chessboard.move({x: 6, y: 1}, {x: 6, y: 2});
            expect(chessboard.getField({x: 6, y: 1}).figure).toBe(whitePawn);
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(blackPawn);

            chessboard.move({x: 6, y: 1}, {x: 6, y: 3});
            expect(chessboard.getField({x: 6, y: 2}).figure).toBe(blackPawn);
            expect(chessboard.getField({x: 6, y: 3}).figure).toBeUndefined();
        });

        xit('pawn beats other figure', () => {
            let chessboard = createChessboard();
            let whitePawn = new Pawn(Color.White);
            let blackPawn = new Pawn(Color.Black);

            chessboard.setFigure({x: 6, y: 1}, whitePawn);
            chessboard.setFigure({x: 5, y: 2}, blackPawn);

            chessboard.move({x: 6, y: 1}, {x: 5, y: 2});
            expect(chessboard.getField({x: 5, y: 2}).figure).toBe(blackPawn);
            expect(chessboard.getField({x: 6, y: 1}).figure).toBeUndefined();
        });
    });

    describe('Test bishop moves', () => {
        it('bishop moves diagonally', () => {
            let chessboard = createChessboard();
            let whiteBishop = new Bishop(Color.White);

            chessboard.setFigure({x: 5, y: 0}, whiteBishop);

            chessboard.move({x: 5, y: 0}, {x: 4, y: 1});
            expect(chessboard.getField({x: 4, y: 1}).figure).toBe(whiteBishop);
            expect(chessboard.getField({x: 5, y: 0}).figure).toBeUndefined();
            
            chessboard.activeColor = Color.White;
            chessboard.move({x: 4, y: 1}, {x: 6, y: 3});
            expect(chessboard.getField({x: 6, y: 3}).figure).toBe(whiteBishop);
            expect(chessboard.getField({x: 4, y: 1}).figure).toBeUndefined();
        });

        xit('blocked bishop cannot move', () => {
            let chessboard = createChessboard();
            let whiteBishop = new Bishop(Color.White);
            let blackPawn = new Bishop(Color.Black);

            chessboard.setFigure({x: 5, y: 0}, whiteBishop);
            chessboard.setFigure({x: 4, y: 1}, blackPawn);

            chessboard.move({x: 5, y: 0}, {x: 3, y: 2});
            expect(chessboard.getField({x: 5, y: 0}).figure).toBe(whiteBishop);
            expect(chessboard.getField({x: 4, y: 1}).figure).toBe(blackPawn);
            expect(chessboard.getField({x: 3, y: 2}).figure).toBeUndefined();
            
            chessboard.move({x: 5, y: 0}, {x: 2, y: 3});
            expect(chessboard.getField({x: 5, y: 0}).figure).toBe(whiteBishop);
            expect(chessboard.getField({x: 4, y: 1}).figure).toBe(blackPawn);
            expect(chessboard.getField({x: 2, y: 3}).figure).toBeUndefined();
        });
    });

    describe('Test knight moves', () => {

    });

    describe('Test rook moves', () => {

    });

    describe('Test king moves', () => {
        it('king plays queenside castling', () => {
            let chessboard = createChessboard();
            let whiteKing = new King(Color.White);
            let whiteRook = new Rook(Color.White);

            chessboard.setFigure({x: 4, y: 0}, whiteKing);
            chessboard.setFigure({x: 0, y: 0}, whiteRook);
            chessboard.move({x: 4, y: 0}, {x: 2, y: 0});

            expect(chessboard.getField({x: 2, y: 0}).figure).toBe(whiteKing);
        });
    
        it('king plays kingside castling', () => {
            let chessboard = createChessboard();
            let whiteKing = new King(Color.White);
            let whiteRook = new Rook(Color.White);

            chessboard.setFigure({x: 4, y: 0}, whiteKing);
            chessboard.setFigure({x: 7, y: 0}, whiteRook);
            chessboard.move({x: 4, y: 0}, {x: 6, y: 0});

            expect(chessboard.getField({x: 6, y: 0}).figure).toBe(whiteKing);
        });
    });

    describe('Test queen moves', () => {

    });
});