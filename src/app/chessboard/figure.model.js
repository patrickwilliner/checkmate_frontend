"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var color_model_1 = require("./color.model");
var position_model_1 = require("./position.model");
var _ = require("lodash");
var Delta = (function () {
    function Delta(x, y) {
        this.x = x;
        this.y = y;
    }
    return Delta;
}());
var Figure = (function () {
    function Figure(shortName, color, imgSrc) {
        this.shortName = shortName;
        this.color = color;
        this.imgSrc = imgSrc;
        this.hasMoved = false;
    }
    Figure.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return delta.x !== 0 || delta.y !== 0;
    };
    Figure.prototype.isBeatingMove = function (from, to) {
        return this.isValidMove(from, to)
            && this.field.chessboard.getField(to).figure
            && this.field.chessboard.getField(to).figure.color !== this.color;
    };
    Figure.prototype.isCheck = function () {
        return false;
    };
    Figure.prototype.isCheckMate = function () {
        return false;
    };
    Figure.prototype.setHasMoved = function () {
        this.hasMoved = true;
    };
    Figure.prototype.getHasMoved = function () {
        return this.hasMoved;
    };
    Figure.prototype.calcDelta = function (from, to) {
        return new Delta(to.x - from.x, to.y - from.y);
    };
    Figure.prototype.isBlockedMove = function (fromPosition, toPosition) {
        if (fromPosition.x === toPosition.x && fromPosition.y === toPosition.y) {
            return false;
        }
        else {
            var nextPosition = new position_model_1.Position();
            if (fromPosition.x !== toPosition.x) {
                nextPosition.x = fromPosition.x > toPosition.x ? fromPosition.x - 1 : fromPosition.x + 1;
            }
            else {
                nextPosition.x = fromPosition.x;
            }
            if (fromPosition.y !== toPosition.y) {
                nextPosition.y = fromPosition.y > toPosition.y ? fromPosition.y - 1 : fromPosition.y + 1;
            }
            else {
                nextPosition.y = fromPosition.y;
            }
            // TODO review
            var nextPositionFigure = this.field.chessboard.rows[nextPosition.y].fields[nextPosition.x].figure;
            if (nextPosition.x === toPosition.x && nextPosition.y === toPosition.y) {
                return nextPositionFigure ? nextPositionFigure.color === this.color : false;
            }
            else if (!_.isNil(nextPositionFigure)) {
                return false;
            }
            else {
                return this.isBlockedMove(nextPosition, toPosition);
            }
        }
    };
    return Figure;
}());
exports.Figure = Figure;
var Pawn = (function (_super) {
    __extends(Pawn, _super);
    function Pawn(color) {
        var _this = _super.call(this, '', color, Pawn.srcMap[color.name]) || this;
        _this.color = color;
        return _this;
    }
    Pawn.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        if (!_super.prototype.isValidMove.call(this, from, to)) {
            return false;
        }
        else if (!this.isForwardMove(delta)) {
            return false;
        }
        else if (this.field.chessboard.getField(to).figure) {
            return false;
        }
        else if (delta.x !== 0) {
            return false;
        }
        else if (this.isBlockedMove(from, to)) {
            return false;
        }
        else if (this.isPawnOpening(from, to)) {
            return true;
        }
        else if (Math.abs(delta.y) > 1) {
            return false;
        }
        else {
            return true;
        }
    };
    Pawn.prototype.isBeatingMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return this.isForwardMove(delta)
            && Math.abs(delta.x) === 1
            && Math.abs(delta.y) === 1
            && _super.prototype.isBeatingMove.call(this, from, to);
    };
    Pawn.prototype.isPawnPromotion = function (from, to) {
        return to.y === 0 || to.y === 7;
    };
    Pawn.prototype.isBlockedMove = function (fromPosition, toPosition) {
        if (fromPosition.x === toPosition.x && fromPosition.y === toPosition.y) {
            var figure = this.field.chessboard.rows[toPosition.y].fields[toPosition.x].figure;
            return !_.isNil(figure);
        }
        else {
            var nextPosition = new position_model_1.Position();
            if (fromPosition.x !== toPosition.x) {
                nextPosition.x = fromPosition.x > toPosition.x ? fromPosition.x - 1 : fromPosition.x + 1;
            }
            else {
                nextPosition.x = fromPosition.x;
            }
            if (fromPosition.y !== toPosition.y) {
                nextPosition.y = fromPosition.y > toPosition.y ? fromPosition.y - 1 : fromPosition.y + 1;
            }
            else {
                nextPosition.y = fromPosition.y;
            }
            var nextPositionFigure = this.field.chessboard.rows[nextPosition.y].fields[nextPosition.x].figure;
            if (!_.isNil(nextPositionFigure)) {
                return true;
            }
            else {
                return this.isBlockedMove(nextPosition, toPosition);
            }
        }
    };
    Pawn.prototype.isPawnOpening = function (from, to) {
        return (this.color === color_model_1.Color.White && from.y === 1 && to.y === 3)
            || (this.color === color_model_1.Color.Black && from.y === 6 && to.y === 4);
    };
    Pawn.prototype.isForwardMove = function (delta) {
        return (this.color === color_model_1.Color.White && delta.y > 0)
            || (this.color === color_model_1.Color.Black && delta.y < 0);
    };
    return Pawn;
}(Figure));
Pawn.srcMap = {
    white: "res/img/chess_plt45.svg",
    black: "res/img/chess_pdt45.svg"
};
exports.Pawn = Pawn;
var Bishop = (function (_super) {
    __extends(Bishop, _super);
    function Bishop(color) {
        var _this = _super.call(this, 'b', color, Bishop.srcMap[color.name]) || this;
        _this.color = color;
        return _this;
    }
    Bishop.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return _super.prototype.isValidMove.call(this, from, to)
            && Math.abs(delta.x) === Math.abs(delta.y)
            && !this.isBlockedMove(from, to);
    };
    return Bishop;
}(Figure));
Bishop.srcMap = {
    white: "res/img/chess_blt45.svg",
    black: "res/img/chess_bdt45.svg"
};
exports.Bishop = Bishop;
var Knight = (function (_super) {
    __extends(Knight, _super);
    function Knight(color) {
        var _this = _super.call(this, 'n', color, Knight.srcMap[color.name]) || this;
        _this.color = color;
        return _this;
    }
    Knight.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return _super.prototype.isValidMove.call(this, from, to)
            && ((Math.abs(delta.x) === 1 && Math.abs(delta.y) === 2)
                || (Math.abs(delta.x) === 2 && Math.abs(delta.y) === 1));
    };
    return Knight;
}(Figure));
Knight.srcMap = {
    white: "res/img/chess_nlt45.svg",
    black: "res/img/chess_ndt45.svg"
};
exports.Knight = Knight;
var Rook = (function (_super) {
    __extends(Rook, _super);
    function Rook(color) {
        var _this = _super.call(this, 'r', color, Rook.srcMap[color.name]) || this;
        _this.color = color;
        return _this;
    }
    Rook.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return _super.prototype.isValidMove.call(this, from, to)
            && (delta.x === 0 || delta.y === 0)
            && !this.isBlockedMove(from, to);
    };
    return Rook;
}(Figure));
Rook.srcMap = {
    white: "res/img/chess_rlt45.svg",
    black: "res/img/chess_rdt45.svg"
};
exports.Rook = Rook;
var Queen = (function (_super) {
    __extends(Queen, _super);
    function Queen(color) {
        var _this = _super.call(this, 'q', color, Queen.srcMap[color.name]) || this;
        _this.color = color;
        return _this;
    }
    Queen.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return _super.prototype.isValidMove.call(this, from, to)
            && (delta.x === 0
                || delta.y === 0
                || Math.abs(delta.x) === Math.abs(delta.y))
            && !this.isBlockedMove(from, to);
    };
    return Queen;
}(Figure));
Queen.srcMap = {
    white: "res/img/chess_qlt45.svg",
    black: "res/img/chess_qdt45.svg"
};
exports.Queen = Queen;
var King = (function (_super) {
    __extends(King, _super);
    function King(color) {
        var _this = _super.call(this, 'k', color, King.srcMap[color.name]) || this;
        _this.color = color;
        return _this;
    }
    King.prototype.isValidMove = function (from, to) {
        var delta = this.calcDelta(from, to);
        return _super.prototype.isValidMove.call(this, from, to)
            && ((Math.abs(delta.x) < 2
                && Math.abs(delta.y) < 2)
                || this.isKingsideCastling(from, to)
                || this.isQueensideCastling(from, to))
            && !this.isBlockedMove(from, to);
    };
    King.prototype.isKingsideCastling = function (from, to) {
        if (this.getHasMoved()) {
            return false;
        }
        else {
            var rookField = this.field.chessboard.getField(new position_model_1.Position(7, from.y));
            return to.x === 6 && rookField.figure && !rookField.figure.getHasMoved() && rookField.figure.color === this.color && (rookField.figure instanceof Rook);
        }
    };
    King.prototype.isQueensideCastling = function (from, to) {
        if (this.getHasMoved()) {
            return false;
        }
        else {
            var rookField = this.field.chessboard.getField(new position_model_1.Position(0, from.y));
            return to.x === 2 && rookField.figure && !rookField.figure.getHasMoved() && rookField.figure.color === this.color && (rookField.figure instanceof Rook);
        }
    };
    return King;
}(Figure));
King.srcMap = {
    white: "res/img/chess_klt45.svg",
    black: "res/img/chess_kdt45.svg"
};
exports.King = King;
//# sourceMappingURL=figure.model.js.map