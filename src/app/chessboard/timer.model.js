"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rx_1 = require("rxjs/Rx");
var Timer = (function () {
    function Timer() {
        this.subject = new Rx_1.Subject();
    }
    Timer.prototype.start = function () {
        var _this = this;
        this.startTime = new Date();
        delete this.stopTime;
        this.timer = Rx_1.Observable.timer(0, 1000);
        this.timer.subscribe(function () { return _this.subject.next(_this.getTotalTimeInSeconds()); });
        return this;
    };
    Timer.prototype.stop = function () {
        this.stopTime = new Date();
        return this;
    };
    Timer.prototype.getTotalTimeInSeconds = function () {
        if (this.stopTime) {
            Math.floor((new Date().getTime() - this.startTime.getTime()) / 1000);
        }
        else if (this.startTime) {
            return Math.floor((new Date().getTime() - this.startTime.getTime()) / 1000);
        }
        else {
            return 0;
        }
    };
    return Timer;
}());
exports.Timer = Timer;
//# sourceMappingURL=timer.model.js.map