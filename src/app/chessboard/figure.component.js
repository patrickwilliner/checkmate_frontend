"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var figure_model_1 = require("./figure.model");
var FigureComponent = (function () {
    function FigureComponent() {
    }
    FigureComponent.prototype.onDragStart = function (event) {
        event.dataTransfer.setData('data', this.figure.field.position.toString());
    };
    return FigureComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", figure_model_1.Figure)
], FigureComponent.prototype, "figure", void 0);
FigureComponent = __decorate([
    core_1.Component({
        selector: '[cm-figure]',
        template: "<img *ngIf=\"figure\" [src]=\"figure.imgSrc\" (dragstart)=\"onDragStart($event)\">"
    })
], FigureComponent);
exports.FigureComponent = FigureComponent;
//# sourceMappingURL=figure.component.js.map