import { Move } from './move.model';
import { King, Pawn, Bishop, Queen } from './figure.model';
import { Color } from './color.model';

describe('<Move> model', () => {
    it('creates string for kingside castling', () => {
        let king = new King(Color.White);
        let move = Move.createKingsideCastling(king);
        expect(move.toString()).toBe('0-0');
    });

    it('creates string for queenside castling', () => {
        let king = new King(Color.White);
        let move = Move.createQueensideCastling(king);
        expect(move.toString()).toBe('0-0-0');
    });

    it('creates string for pawn move', () => {
        let pawn = new Pawn(Color.White);
        let move = Move.createSimpleMove(pawn, {x: 0, y: 6}, {x: 0, y: 5});
        expect(move.toString()).toBe('a3');
    });

    it('creates string for pawn beating move', () => {
        let pawn = new Pawn(Color.White);
        let move = Move.createBeatingMove(pawn, {x: 0, y: 6}, {x: 1, y: 5});
        expect(move.toString()).toBe('axb3');
    });

    it('creates string for bishop move', () => {
        let pawn = new Bishop(Color.White);
        let move = Move.createSimpleMove(pawn, {x: 0, y: 0}, {x: 7, y: 7});
        expect(move.toString()).toBe('Bh1');
    });

    it('creates string for queen beating move', () => {
        let pawn = new Queen(Color.White);
        let move = Move.createBeatingMove(pawn, {x: 0, y: 0}, {x: 7, y: 7});
        expect(move.toString()).toBe('Qxh1');
    });
});