"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var chessboard_model_1 = require("./chessboard.model");
var ChessboardService = (function () {
    function ChessboardService(modalService) {
        this.modalService = modalService;
        this.chessboard = new chessboard_model_1.Chessboard(this);
    }
    ChessboardService.prototype.registerPawnPromotionDialog = function (pawnPromotionDialogComponent) {
        this.pawnPromotionDialogComponent = pawnPromotionDialogComponent;
    };
    ChessboardService.prototype.newGame = function () {
        this.chessboard.init();
        this.chessboard.start();
    };
    ChessboardService.prototype.getChessboard = function () {
        return this.chessboard;
    };
    ChessboardService.prototype.showPawnPromotionDialog = function () {
        console.log('xxx');
        this.modalService.open(this.pawnPromotionDialogComponent);
    };
    return ChessboardService;
}());
ChessboardService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal])
], ChessboardService);
exports.ChessboardService = ChessboardService;
//# sourceMappingURL=chessboard.service.js.map