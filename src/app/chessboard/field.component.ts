import { Component, Input } from '@angular/core';

import { Color } from './color.model';
import { Field } from './field.model';
import { Figure } from './figure.model';
import { Position } from './position.model';

@Component({
  selector: '[cm-field]',
  template: `
    <div class="field {{field.color.cssClass}} {{validMoveClass}}" (drop)="onDrop($event)" (dragover)="onDragOver($event)" (dragleave)="onDragLeave($event)">
      <div cm-figure [figure]="field.figure"></div>
    </div>
  `
})
export class FieldComponent {
  @Input() field: Field;
  validMoveClass: string = '';

  onDrop(event: any): void {
    let data: string = event.dataTransfer.getData('data');
    this.validMoveClass = '';
    this.field.chessboard.move(Position.fromString(data), this.field.position);
    event.preventDefault();
  }

  onDragOver(event: any): void {
    this.validMoveClass = 'validMove';
    event.preventDefault();
  }

  onDragLeave(event: any): void {
    this.validMoveClass = '';
    event.preventDefault();
  }
}
