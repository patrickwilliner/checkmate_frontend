"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var chessboard_service_1 = require("./chessboard.service");
var pawn_promotion_dialog_component_1 = require("./pawn-promotion.dialog.component");
var logger_service_1 = require("../util/logger.service");
var ChessboardComponent = (function () {
    function ChessboardComponent(chessboardService, log) {
        this.chessboardService = chessboardService;
        this.log = log;
    }
    return ChessboardComponent;
}());
__decorate([
    core_1.ViewChild(pawn_promotion_dialog_component_1.PawnPromotionDialogComponent),
    __metadata("design:type", pawn_promotion_dialog_component_1.PawnPromotionDialogComponent)
], ChessboardComponent.prototype, "pawnPromotionDialog", void 0);
ChessboardComponent = __decorate([
    core_1.Component({
        selector: '[cm-chessboard]',
        styleUrls: ['./chessboard.component.css'],
        template: "\n    <div cm-pawn-promotion-dialog></div>\n\n    <div class=\"chessboard\">\n      <div class=\"row label\">\n        <div class=\"field label\"></div>\n        <div class=\"field\">A</div>\n        <div class=\"field\">B</div>\n        <div class=\"field\">C</div>\n        <div class=\"field\">D</div>\n        <div class=\"field\">E</div>\n        <div class=\"field\">F</div>\n        <div class=\"field\">G</div>\n        <div class=\"field\">H</div>\n      </div>\n\n      <div class=\"row\" *ngFor=\"let row of chessboardService.getChessboard().rows; let i = index;\">\n        <div class=\"field label\"><div>{{8 - i}}</div></div>\n        <div cm-field *ngFor=\"let field of row.fields\" [field]=\"field\"></div>\n      </div>\n    </div>\n  "
    }),
    __metadata("design:paramtypes", [chessboard_service_1.ChessboardService, logger_service_1.LoggerService])
], ChessboardComponent);
exports.ChessboardComponent = ChessboardComponent;
//# sourceMappingURL=chessboard.component.js.map