"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timer_model_1 = require("./timer.model");
describe('<Timer> model', function () {
    it('creates a timer without starting it', function () {
        var timer = new timer_model_1.Timer();
        expect(timer.getTotalTimeInSeconds()).toBe(timer.getTotalTimeInSeconds());
    });
    it('starts the timer', function () {
        var timer = new timer_model_1.Timer().start();
        var firstTimeMeasure = timer.getTotalTimeInSeconds();
        setTimeout(function () {
            expect(timer.getTotalTimeInSeconds()).not.toBe(firstTimeMeasure);
        }, 1000);
    });
    it('stops the timer', function () {
        var timer = new timer_model_1.Timer().start().stop();
        var firstTimeMeasure = timer.getTotalTimeInSeconds();
        setTimeout(function () {
            expect(timer.getTotalTimeInSeconds()).toBe(firstTimeMeasure);
        }, 1000);
    });
});
//# sourceMappingURL=timer.model.spec.js.map