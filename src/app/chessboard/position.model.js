"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Position = (function () {
    function Position(x, y) {
        this.x = x;
        this.y = y;
        // TODO assert range
    }
    Position.prototype.toString = function () {
        return JSON.stringify(this);
    };
    Position.fromString = function (value) {
        return JSON.parse(value);
    };
    return Position;
}());
exports.Position = Position;
//# sourceMappingURL=position.model.js.map