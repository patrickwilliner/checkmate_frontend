import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ChessboardComponent } from './chessboard.component';
import { FieldComponent } from './field.component';
import { ColorComponent } from './color.component';
import { FigureComponent } from './figure.component';
import { ChessboardService } from './chessboard.service';
import { PawnPromotionDialogComponent } from './pawn-promotion.dialog.component';

@NgModule({
  imports: [CommonModule, NgbModule],
  declarations: [ChessboardComponent, FieldComponent, ColorComponent, FigureComponent, PawnPromotionDialogComponent],
  exports: [ChessboardComponent],
  providers: [ChessboardService]
})
export class ChessboardModule {
}
