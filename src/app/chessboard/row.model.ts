import { Injectable } from '@angular/core';

import { Field } from './field.model';

@Injectable()
export class Row {
  fields: Field[] = [];
}
