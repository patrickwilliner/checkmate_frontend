import { Injectable } from '@angular/core';

import { Figure } from './figure.model';
import { Color } from './color.model';
import { Chessboard } from './chessboard.model';
import { Position } from './position.model';

@Injectable()
export class Field {
  constructor(public position: Position, public color: Color, public chessboard: Chessboard, public figure?: Figure) {
  }
}
