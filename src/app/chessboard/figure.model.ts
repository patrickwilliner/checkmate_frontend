import { Color } from './color.model';
import { Field } from './field.model';
import { Position } from './position.model';
import * as _ from 'lodash';

class Delta {
  constructor(public x: number, public y: number) {
  }
}

abstract class Figure {
  private hasMoved = false;
  public field: Field;

  constructor(public shortName: string, public color: Color, public imgSrc: string) {
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);
    return delta.x !== 0 || delta.y !== 0;
  }

  isBeatingMove(from: Position, to: Position): boolean {
    return this.isValidMove(from, to)
      && this.field.chessboard.getField(to).figure
      && this.field.chessboard.getField(to).figure.color !== this.color;
  }

  isCheck() {
    return false;
  }

  isCheckMate() {
    return false;
  }

  setHasMoved() {
    this.hasMoved = true;
  }

  getHasMoved() {
    return this.hasMoved;
  }

  protected calcDelta(from: Position, to: Position): Delta {
    return new Delta(to.x - from.x, to.y - from.y);
  }

  protected isBlockedMove(fromPosition: Position, toPosition: Position): boolean {
    if (fromPosition.x === toPosition.x && fromPosition.y === toPosition.y) {
      return false;
    } else {
      var nextPosition = new Position();

      if (fromPosition.x !== toPosition.x) {
        nextPosition.x = fromPosition.x > toPosition.x ? fromPosition.x - 1 : fromPosition.x + 1;
      } else {
        nextPosition.x = fromPosition.x;
      }

      if (fromPosition.y !== toPosition.y) {
        nextPosition.y = fromPosition.y > toPosition.y ? fromPosition.y - 1 : fromPosition.y + 1;
      } else {
        nextPosition.y = fromPosition.y;
      }

      // TODO review
      let nextPositionFigure = this.field.chessboard.rows[nextPosition.y].fields[nextPosition.x].figure;

      if (nextPosition.x === toPosition.x && nextPosition.y === toPosition.y) {
        return nextPositionFigure ? nextPositionFigure.color === this.color : false;
      } else if (!_.isNil(nextPositionFigure)) { 
        return false; 
      } else {
        return this.isBlockedMove(nextPosition, toPosition);
      }
    }
  }
}

class Pawn extends Figure {
  private static srcMap = {
    white: "assets/img/chess_plt45.svg",
    black: "assets/img/chess_pdt45.svg"
  }

  constructor(public color: Color) {
    super('', color, Pawn.srcMap[color.name]);
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);

    if (!super.isValidMove(from, to)) {
      return false;
    } else if (!this.isForwardMove(delta)) {
      return false;
    } else if (this.field.chessboard.getField(to).figure) {
      return false;
    } else if (delta.x !== 0) {
      return false;
    } else if (this.isBlockedMove(from, to)) {
      return false;
    } else if (this.isPawnOpening(from, to)) {
      return true;
    } else if (Math.abs(delta.y) > 1) {
      return false;
    } else {
      return true;
    }
  }

  isBeatingMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);

    return this.isForwardMove(delta)
      && Math.abs(delta.x) === 1
      && Math.abs(delta.y) === 1
      && super.isBeatingMove(from, to);
  }

  isPawnPromotion(from: Position, to: Position) {
    return to.y === 0 || to.y === 7;
  }

  protected isBlockedMove(fromPosition: Position, toPosition: Position): boolean {
    if (fromPosition.x === toPosition.x && fromPosition.y === toPosition.y) {
      let figure = this.field.chessboard.rows[toPosition.y].fields[toPosition.x].figure;
      return !_.isNil(figure);
    } else {
      var nextPosition = new Position();

      if (fromPosition.x !== toPosition.x) {
        nextPosition.x = fromPosition.x > toPosition.x ? fromPosition.x - 1 : fromPosition.x + 1;
      } else {
        nextPosition.x = fromPosition.x;
      }

      if (fromPosition.y !== toPosition.y) {
        nextPosition.y = fromPosition.y > toPosition.y ? fromPosition.y - 1 : fromPosition.y + 1;
      } else {
        nextPosition.y = fromPosition.y;
      }
      
      let nextPositionFigure = this.field.chessboard.rows[nextPosition.y].fields[nextPosition.x].figure;
      if (!_.isNil(nextPositionFigure)) {
        return true;
      } else {
        return this.isBlockedMove(nextPosition, toPosition);
      }
    }
  }

  private isPawnOpening(from: Position, to: Position) {
    return (this.color === Color.White && from.y === 1 && to.y === 3)
      || (this.color === Color.Black && from.y === 6 && to.y === 4);
  }

  private isForwardMove(delta: Delta) {
    return (this.color === Color.White && delta.y > 0)
      || (this.color === Color.Black && delta.y < 0);
  }
}

class Bishop extends Figure {
  private static srcMap = {
    white: "assets/img/chess_blt45.svg",
    black: "assets/img/chess_bdt45.svg"
  }

  constructor(public color: Color) {
    super('b', color, Bishop.srcMap[color.name]);
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);
    return super.isValidMove(from, to)
      && Math.abs(delta.x) === Math.abs(delta.y)
      && !this.isBlockedMove(from, to);
  }
}

class Knight extends Figure {
  private static srcMap = {
    white: "assets/img/chess_nlt45.svg",
    black: "assets/img/chess_ndt45.svg"
  }

  constructor(public color: Color) {
    super('n', color, Knight.srcMap[color.name]);
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);
    return super.isValidMove(from, to)
      && ((Math.abs(delta.x) === 1 && Math.abs(delta.y) === 2)
      || (Math.abs(delta.x) === 2 && Math.abs(delta.y) === 1));
  }
}

class Rook extends Figure {
  private static srcMap = {
    white: "assets/img/chess_rlt45.svg",
    black: "assets/img/chess_rdt45.svg"
  }

  constructor(public color: Color) {
    super('r', color, Rook.srcMap[color.name]);
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);
    return super.isValidMove(from, to)
      && (delta.x === 0 || delta.y === 0)
      && !this.isBlockedMove(from, to);
  }
}

class Queen extends Figure {
  private static srcMap = {
    white: "assets/img/chess_qlt45.svg",
    black: "assets/img/chess_qdt45.svg"
  }

  constructor(public color: Color) {
    super('q', color, Queen.srcMap[color.name]);
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);

    return super.isValidMove(from, to)
      && (delta.x === 0
      || delta.y === 0
      || Math.abs(delta.x) === Math.abs(delta.y))
      && !this.isBlockedMove(from, to);
  }
}

class King extends Figure {
  private static srcMap = {
    white: "assets/img/chess_klt45.svg",
    black: "assets/img/chess_kdt45.svg"
  }

  constructor(public color: Color) {
    super('k', color, King.srcMap[color.name]);
  }

  isValidMove(from: Position, to: Position): boolean {
    let delta: Delta = this.calcDelta(from, to);
    return super.isValidMove(from, to)
      && ((Math.abs(delta.x) < 2
      && Math.abs(delta.y) < 2)
      || this.isKingsideCastling(from, to)
      || this.isQueensideCastling(from, to))
      && !this.isBlockedMove(from, to);
  }

  isKingsideCastling(from: Position, to: Position) {
    if (this.getHasMoved()) {
      return false;
    } else {
      let rookField = this.field.chessboard.getField(new Position(7, from.y));
      return to.x === 6 && rookField.figure && !rookField.figure.getHasMoved() &&  rookField.figure.color === this.color && (rookField.figure instanceof Rook);
    }
  }

  isQueensideCastling(from: Position, to: Position) {
    if (this.getHasMoved()) {
      return false;
    } else {
      let rookField = this.field.chessboard.getField(new Position(0, from.y));
      return to.x === 2 && rookField.figure && !rookField.figure.getHasMoved() && rookField.figure.color === this.color && (rookField.figure instanceof Rook);
    }
  }
}

export { Figure, Pawn, Bishop, Knight, Rook, Queen, King}
