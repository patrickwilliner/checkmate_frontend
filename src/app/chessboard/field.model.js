"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var figure_model_1 = require("./figure.model");
var color_model_1 = require("./color.model");
var chessboard_model_1 = require("./chessboard.model");
var position_model_1 = require("./position.model");
var Field = (function () {
    function Field(position, color, chessboard, figure) {
        this.position = position;
        this.color = color;
        this.chessboard = chessboard;
        this.figure = figure;
    }
    return Field;
}());
Field = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [position_model_1.Position, color_model_1.Color, chessboard_model_1.Chessboard, figure_model_1.Figure])
], Field);
exports.Field = Field;
//# sourceMappingURL=field.model.js.map