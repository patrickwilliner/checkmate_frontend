"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var chessboard_component_1 = require("./chessboard.component");
var field_component_1 = require("./field.component");
var color_component_1 = require("./color.component");
var figure_component_1 = require("./figure.component");
var chessboard_service_1 = require("./chessboard.service");
var pawn_promotion_dialog_component_1 = require("./pawn-promotion.dialog.component");
var ChessboardModule = (function () {
    function ChessboardModule() {
    }
    return ChessboardModule;
}());
ChessboardModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule, ng_bootstrap_1.NgbModule],
        declarations: [chessboard_component_1.ChessboardComponent, field_component_1.FieldComponent, color_component_1.ColorComponent, figure_component_1.FigureComponent, pawn_promotion_dialog_component_1.PawnPromotionDialogComponent],
        exports: [chessboard_component_1.ChessboardComponent],
        providers: [chessboard_service_1.ChessboardService]
    })
], ChessboardModule);
exports.ChessboardModule = ChessboardModule;
//# sourceMappingURL=chessboard.module.js.map