import { Row } from './row.model';
import { Figure, Pawn, Rook, Bishop, Knight, King, Queen } from './figure.model';
import { Position } from './position.model';
import { Field } from './field.model';
import { Color } from './color.model';
import { Timer } from './timer.model';
import { Move } from './move.model';
import { ChessboardService } from './chessboard.service';
import * as _ from 'lodash';

export class Chessboard {
  public rows: Row[] = [];
  public activeColor = Color.White;
  public timer = new Timer();
  public history: Move[] = [];

  constructor(private chessboardService: ChessboardService) {
    this.buildChessboard();
  }

  public start() {
    this.timer.start();
  }

  public setFigure(position: Position, figure: Figure) {
    let field: Field = this.getField(position);

    if (figure) {
      figure.field = field;
    }
    field.figure = figure;
  }

  getField(position: Position): Field {
    return this.rows[position.y].fields[position.x];
  }

  move(from: Position, to: Position): void {
    let figure: Figure = this.getField(from).figure;

    if (figure instanceof King) {
      if (figure.isKingsideCastling(from, to)) {
        this.setFigure(new Position(5, from.y), this.getField(new Position(7, from.y)).figure);
        this.setFigure(new Position(7, from.y), undefined);
        this.setFigure(to, figure);
        this.setFigure(from, undefined);
        this.activeColor = this.activeColor.getOther();
        this.history.push(Move.createKingsideCastling(figure));
        figure.setHasMoved();
        return;
      } else if (figure.isQueensideCastling(from, to)) {
        this.setFigure(new Position(3, from.y), this.getField(new Position(0, from.y)).figure);
        this.setFigure(new Position(0, from.y), undefined);
        this.setFigure(to, figure);
        this.setFigure(from, undefined);
        this.activeColor = this.activeColor.getOther();
        this.history.push(Move.createQueensideCastling(figure));
        figure.setHasMoved();
        return;
      }
    }

    if (this.isBeatingMove(from, to)) {
      this.setFigure(to, figure);
      this.setFigure(from, undefined);
      this.activeColor = this.activeColor.getOther();
      this.history.push(new Move(figure, from, to, true));
      figure.setHasMoved();
      this.checkAndHandlePawnPromotion(figure, from, to);
    } else if (this.isValidMove(from, to)) {
      this.setFigure(to, figure);
      this.setFigure(from, undefined);
      this.activeColor = this.activeColor.getOther();
      this.history.push(new Move(figure, from, to));
      figure.setHasMoved();
      this.checkAndHandlePawnPromotion(figure, from, to);
    }
  }

  checkAndHandlePawnPromotion(figure: Figure, from: Position, to: Position) {
    if ((figure instanceof Pawn) && figure.isPawnPromotion(from, to)) {
      this.chessboardService.showPawnPromotionDialog();
    }
  }

  isBeatingMove(from: Position, to: Position) {
    let figure: Figure = this.getField(from).figure;
    return figure.isBeatingMove(from, to);
  }

  isValidMove(from: Position, to: Position): boolean {
    let figure: Figure = this.getField(from).figure;
    return figure.color === this.activeColor && figure.isValidMove(from, to);
  }

  init() {
    this.activeColor = Color.White;
    this.rows = [];
    this.buildChessboard();
    this.initFigures();
    this.history = [];
    this.timer = new Timer();
  }

  initFigures() {
    this.initWhiteFigures();
    this.initBlackFigures();
  }

  toString() {
    let s = '\n  0 1 2 3 4 5 6 7\n -----------------\n';
    
    for (let i = 0; i < this.rows.length; i++) {
      let row = this.rows[i];

      s += i + '|' + _.map(row.fields, field => {
        if (field.figure && field.figure.color === Color.White) {
          return (field.figure.shortName || 'p').toUpperCase();
        } else if (field.figure && field.figure.color === Color.Black) {
          return (field.figure.shortName || 'p').toLowerCase();
        } else {
          return ' ';
        }
      }).join('|') + '|\n';
    }

    return s;
  }

  private buildChessboard() {
    // TODO turn board (starting field color should be white and black plays on top)
    let currentColor = Color.White;

    for (let y = 0; y < 8; y++) {
      let row = new Row();
      this.rows.push(row);

      currentColor = currentColor.getOther();

      for (let x = 0; x < 8; x++) {
        row.fields.push(new Field(new Position(x, y), currentColor, this));
        currentColor = currentColor.getOther();
      }
    }
  }

  private initWhiteFigures(): void {
    this.setFigure(new Position(0, 0), new Rook(Color.White));
    this.setFigure(new Position(1, 0), new Knight(Color.White));
    this.setFigure(new Position(2, 0), new Bishop(Color.White));
    this.setFigure(new Position(3, 0), new Queen(Color.White));
    this.setFigure(new Position(4, 0), new King(Color.White));
    this.setFigure(new Position(5, 0), new Bishop(Color.White));
    this.setFigure(new Position(6, 0), new Knight(Color.White));
    this.setFigure(new Position(7, 0), new Rook(Color.White));

    for (let i = 0; i < 8; i++) {
      this.setFigure(new Position(i, 1), new Pawn(Color.White));
    }
  }

  private initBlackFigures() : void {
    for (let i = 0; i < 8; i++) {
      this.setFigure(new Position(i, 6), new Pawn(Color.Black));
    }

    this.setFigure(new Position(0, 7), new Rook(Color.Black));
    this.setFigure(new Position(1, 7), new Knight(Color.Black));
    this.setFigure(new Position(2, 7), new Bishop(Color.Black));
    this.setFigure(new Position(3, 7), new Queen(Color.Black));
    this.setFigure(new Position(4, 7), new King(Color.Black));
    this.setFigure(new Position(5, 7), new Bishop(Color.Black));
    this.setFigure(new Position(6, 7), new Knight(Color.Black));
    this.setFigure(new Position(7, 7), new Rook(Color.Black));
  }
}
