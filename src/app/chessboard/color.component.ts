import { Component, Input } from '@angular/core';

import { Color } from './color.model';

@Component({
  selector: '[cm-color]',
  template: `<div style="background-color: black;"></div>`
})
export class ColorComponent {
  @Input() color: Color;
}
