"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var figure_model_1 = require("./figure.model");
var Move = (function () {
    function Move(figure, from, to, beat) {
        this.figure = figure;
        this.from = from;
        this.to = to;
        this.beat = beat;
    }
    Move.prototype.toString = function () {
        if (this.isKingsideCastling) {
            return '0-0';
        }
        else if (this.isQueensideCastling) {
            return '0-0-0';
        }
        else if (this.beat && (this.figure instanceof figure_model_1.Pawn)) {
            return String.fromCharCode(97 + this.from.x) + 'x' + String.fromCharCode(97 + this.to.x) + (8 - this.to.y);
        }
        else if (this.beat) {
            return this.figure.shortName.toUpperCase() + 'x' + String.fromCharCode(97 + this.to.x) + (8 - this.to.y);
        }
        else {
            return this.figure.shortName.toUpperCase() + String.fromCharCode(97 + this.to.x) + (8 - this.to.y);
        }
    };
    Move.createSimpleMove = function (figure, from, to) {
        var move = new Move(figure);
        move.from = from;
        move.to = to;
        return move;
    };
    Move.createBeatingMove = function (figure, from, to) {
        var move = new Move(figure);
        move.from = from;
        move.to = to;
        move.beat = true;
        return move;
    };
    Move.createKingsideCastling = function (figure) {
        var move = new Move(figure);
        move.isKingsideCastling = true;
        return move;
    };
    Move.createQueensideCastling = function (figure) {
        var move = new Move(figure);
        move.isQueensideCastling = true;
        return move;
    };
    Move.createPawnPromotion = function (pawn, from, to) {
        var move = new Move(pawn);
        move.isPawnPromotion = true;
        return move;
    };
    return Move;
}());
exports.Move = Move;
//# sourceMappingURL=move.model.js.map