"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var chessboard_service_1 = require("./chessboard.service");
var PawnPromotionDialogComponent = (function () {
    function PawnPromotionDialogComponent(chessboardService) {
        this.chessboardService = chessboardService;
    }
    PawnPromotionDialogComponent.prototype.ngAfterViewInit = function () {
        this.chessboardService.registerPawnPromotionDialog(this.templateElement);
    };
    return PawnPromotionDialogComponent;
}());
__decorate([
    core_1.ViewChild('template'),
    __metadata("design:type", core_1.ElementRef)
], PawnPromotionDialogComponent.prototype, "templateElement", void 0);
PawnPromotionDialogComponent = __decorate([
    core_1.Component({
        selector: '[cm-pawn-promotion-dialog]',
        template: "\n    <ng-template #template>\n      <div ok-dialog title=\"Select\" okBtnLabel=\"oKiDoKi\">\n        <div class=\"row\">\n          <div class=\"col-3\"><img src=\"res/img/chess_qlt45.svg\"></div>\n          <div class=\"col-3\"><img src=\"res/img/chess_nlt45.svg\"></div>\n          <div class=\"col-3\"><img src=\"res/img/chess_blt45.svg\"></div>\n          <div class=\"col-3\"><img src=\"res/img/chess_rlt45.svg\"></div>\n        </div>\n      </div>\n    </ng-template>\n  "
    }),
    __metadata("design:paramtypes", [chessboard_service_1.ChessboardService])
], PawnPromotionDialogComponent);
exports.PawnPromotionDialogComponent = PawnPromotionDialogComponent;
//# sourceMappingURL=pawn-promotion.dialog.component.js.map