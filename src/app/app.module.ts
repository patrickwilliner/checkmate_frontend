import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NavigationModule } from './navigation/navigation.module';
import { ChessboardModule } from './chessboard/chessboard.module';
import { UtilModule } from './util/util.module';
import { GameModule } from './game/game.module';
import { SessionModule } from './session/session.module';
import { DialogModule } from './dialog/dialog.module';

import { AppComponent } from './app.component';

@NgModule({
  imports:      [BrowserModule, NgbModule.forRoot(), NavigationModule, ChessboardModule, UtilModule, GameModule, SessionModule, DialogModule],
  declarations: [AppComponent],
  bootstrap:    [AppComponent]
})
export class AppModule {
}
